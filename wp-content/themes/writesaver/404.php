
<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage writesaver
 * @since writesaver 1.0
 */
get_header();
?>
<section>
    <div class="breadcum">
        <div class="container">
            <div class="page_title">
                <h1><?php _e('404', 'writesaver'); ?></h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="not_found error_404_page contact_main">
        <div class="container">
            <img src="<?php echo get_template_directory_uri(); ?>/images/error_404_smile1.png" alt="smile" class="img-responsive error_404_smile">
            <h1>404</h1>
            <p>Something is wrong</p>
            <span>The page you are looking for was moved, removed, renamed or simply might never have existed.</span>
            <a href="<?php echo home_url(); ?>" class="btn_sky">Go to Home</a>
        </div>
    </div>
</section>
<?php get_footer(); ?>
