<?php

/*
 * Template Name: Customer Dashboard Free Track
 */

get_header();
$current_user = wp_get_current_user();
$user_roles_array = $current_user->roles;
$user_role = array_shift($user_roles_array);
if (!is_user_logged_in() || $user_role != "customer") {
    echo '<script>window.location.href="' . get_site_url() . '"</script>';
    exit;
}
?>
<section>

    <div class="breadcum">

        <div class="container">

            <div class="page_title">

                <h1>Dashboad Test</h1>

            </div>

        </div>

    </div>

</section>

<section class="uploaded_file_main">

    <div class="clickable">

        <a href="javascript:void(0);" class="openre">View all submitted documents <i class="fa fa-angle-down" aria-hidden="true"></i></a>                

    </div>

    <div class="collapsible_content" style="display: none;">

        <div class="inner_content">

            <div class="container">

                <div class="row">

                    <div class="col-sm-2">

                        <div class="full_content">

                            <div class="full_content_header">

                                <p>

                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas efficitur et dolor et rhoncus. Integer accumsan ultrices est,maximus mattis

                                </p>

                            </div>

                            <div class="status">



                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <div class="clickable">

                <a href="javascript:void(0);" class="closer">Close <i class="fa fa-angle-up" aria-hidden="true"></i></a>                

            </div>

        </div>                

    </div>

</section>

<section>

    <div class="container">

        <div class="privacy customer">

            <div class="row service">

                <div class="col-sm-5">

                    <div class="total_ammount credit">

                        <div class="left">

                            <h4>150<span>Words</span></h4>

                            <p>Remaining credit<a href="#">Upgrade Plan</a></p>                                    

                        </div>

                        <div class="right"></div>

                    </div>

                </div>

                <div class="col-sm-5">

                    <div class="total_ammount submitted">

                        <div class="left">

                            <h4>0<span>Docs</span></h4>

                            <p>Total submitted<a href="#">View all</a></p>                                    

                        </div>

                        <div class="right"></div>

                    </div>

                </div>

                <div class="col-sm-2">

                    <label class="cloud_file">

                        Upload Your Doc.

                        <input type="file" />

                    </label>

                </div>

            </div>

            <div class="doc_name">

                <h2>Doc name10</h2>

            </div>
            <div class="used_word">

                <span>Doc Words:</span><span class="count" id="wordCount">0</span>

            </div>

            <div id="textarea" contenteditable="true" data-text="Type or paste (Ctrl+V) your text here. Upload a document to save original formatting." >
                <b data-id="10_5_14_Lorem" id="a_10_5_14">Lorem</b> <b data-id="15_5_19_ipsum" id="a_15_5_19">ipsum</b> dolor sit amet, consectetur adipiscing elit. Cras tempus leo turpis, quis congue lorem ultrices non. <b data-id="50_6_55_Mauris" id="a_50_6_55">Mauris</b> pharetra vestibulum ex,

                in tristique diam fringilla ultrices. Donec scelerisque nulla quis risus venenatis mattis sit amet ut felis. Donec consectetur mauris lorem, sit amet fringilla massa luctus ut.

                Cras nec urna et urna tristique molestie. Phasellus eros libero, venenatis quis risus nec, vulputate molestie erat. Phasellus non condimentum diam. Fusce tristique, urna et

                egestas congue, ante justo porttitor neque, quis imperdiet nulla nisi vel elit. Vestibulum egestas justo in leo facilisis laoreet. Nam ex dui, rhoncus ut libero vitae, 

                ullamcorper sodales mauris. Nullam et risus vel sem tristique molestie. Duis consectetur libero quis turpis posuere, a vulputate ipsum imperdiet. Duis bibendum sollicitudin 

                ante at congue. Maecenas lobortis eros a tellus condimentum laoreet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut volutpat odio

                nec nibh scelerisque facilisis.
            </div>
            <div id="suggestions" style="display:none;">

            </div>
            <div class="submit_area">

                <p>Your submission has been proofread by our free automated software. To have our proofreading team read your paper, just click below!</p>

                <div class="btn_blue">

                    <a href="#edit_free" class="btn_sky pop_btn">Edit Now Free</a>

                </div>                            

                <div id="edit_free" class="pop_overlay" style="display: none;">

                    <div class="pop_main">

                        <div class="pop_head">

                            <a href="javascript:void(0);"><i class="fa fa-remove" aria-hidden="true"></i></a>

                        </div>

                        <div class="pop_body">

                            <div class="page_title">

                                <h2>Please note</h2>

                            </div>

                            <div class="pop_content">

                                <p>

                                    This automated service is not perfect, and the only way to have perfect writing is to have a proofread from anative.

                                </p>

                                <div class="btn_blue">

                                    <a href="#" class="btn_sky">Try it Now</a>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>
        </div>

    </div>
    <input type="hidden" id="hdntextgearapikey" name="hdntextgearapikey" value="bqLkrNkrLebeqp8v" /> 


    <div id="resultofgrammer">
    </div>
    <span id="spn_Grammar_Spelling_issues" />
    <span id="textgearapiscores" />
    <span id="grammercheck_span_GrammarErorrosTextgearAPI" />
    <span id="grammercheck_span" />
    <span id="grammercheck_span_error" />
    <span id="lblResultsname"></span>
</section>
<style>#textarea b{
        font-weight: normal;
        background: green;
        color: white;
    }</style>
<script>
    $('body').on("click", '#textarea b', function ()
    {
        var id = $(this).attr("id");
        $("#suggestions").html("");

        if (id == "a_10_5_14")
        {
            var wrongword = "Lorem";
            var suggetions = "Loremas";
            var str = "<div><div><strike>" + wrongword + "</strike></div>";
            str += "<span>" + suggetions + "<span>";
            str += "</div>";
            $("#suggestions").html(str);
            $("#suggestions").show();
        } else if (id == "a_15_5_19")
        {

            var wrongword = "ipsum";
            var suggetions = "ipsumas";
            var suggetions1 = "ipsumes";
            var str = "<div><div><strike>" + wrongword + "</strike></div>";
            str += "<span>" + suggetions + "<span>";
            str += "<span>" + suggetions1 + "<span>";
            str += "</div>";
            $("#suggestions").html(str);
            $("#suggestions").show();
        } else {
            var wrongword = "Mauris";
            var suggetions = "Maurisies";
            var str = "<div><div><strike>" + wrongword + "</strike></div>";
            str += "<span>" + suggetions + "<span>";
            str += "</div>";
            $("#suggestions").html(str);
            $("#suggestions").show();
        }

    });

    function findAndReplacegrammar(string, target, replacement) {

        if ($('#textarea:contains(' + replacement + ')').length == 0) {

            var lyrics = string;
            var find = target;
            var spantag = replacement;
            var regex = new RegExp('\\b' + find + '\\b', "g");


            return lyrics.replace(regex, spantag);
        } else {
            return "";
        }

    }
    function onTestChange() {

        //var key = window.event.keyCode;

        //// If the user has pressed enter
        //if (key === 13) {

        //    document.getElementById("textarea").value = document.getElementById("textarea").value + "\n*";

        //    return false;
        //}
        //else {
        //    return true;
        //}
        characters_sentence_word_count();
    }
    counter = function () {
        var value = $('#textarea').text();

        if (value.length == 0) {
            $('#wordCount').html(0);
            $('#totalChars').html(0);
            return;
        }

        characters_sentence_word_count();
    };

    $(document).ready(function () {
        $('#textarea').change(counter);
        $('#textarea').keydown(counter);
        $('#textarea').keypress(counter);
        $('#textarea').keyup(counter);
        $('#textarea').blur(counter);
        $('#textarea').focus(counter);
        $('#textarea').mouseleave(counter);
    });
    function characters_sentence_word_count() {
        debugger;
        var value = $("#textarea").text();
        //  $("#EssayText").val(value);
        var regex = /\s+/gi;
        var wordCount = value.trim().replace(regex, ' ').split(' ').length;
        var totalChars = value.length;
        var charCount = value.trim().length;
        var charCountNoSpace = value.replace(regex, '').length;
        // var sentencecount = value.replace(regex, '.').length;
        //  $("#totalChars").html(charCount);
        $('#wordCount').html(wordCount);
        // $('#sentenceCount').html(sentencecount);
        var sentancecount = 0;
        var fullStopCount = value.match(/\./g);
        if (fullStopCount != null && fullStopCount != "") {
            fullStopCount = fullStopCount.length;
            sentancecount = fullStopCount;
        } else {
            sentancecount = 0;
        }
        var fullquestions = value.match(/\?/g);
        if (fullquestions != null && fullquestions != "") {
            fullquestions = fullquestions.length;
            sentancecount = parseInt(sentancecount) + parseInt(fullquestions);
        } else {
            sentancecount = parseInt(sentancecount) + parseInt(0);
        }

        var fullexplaintions = value.match(/\!/g);

        if (fullexplaintions != null && fullexplaintions != "") {
            fullexplaintions = fullexplaintions.length;
            sentancecount = parseInt(sentancecount) + parseInt(fullexplaintions);
        } else {
            sentancecount = parseInt(sentancecount) + parseInt(0);
        }
        //$('#sentenceCount').html(sentancecount);
    }

    function grammarCheck()
    {

        var val = $('#textarea').text();
        // $("#EssayText").val($("#textarea").text());

        //  $("#loading1").show();
        var mainarryas = [];
        var apiurl = "https://api.textgears.com/check.php?text=" + encodeURIComponent(val) + "&key=" + $("#hdntextgearapikey").val();
        $.ajax({
            type: "GET",
            url: apiurl,
            dataType: "json",
            // async:true,
            method: "post",
            success: function (data) {
                var htm1111;
                $('#grammercheck_span').html("");
                $('#grammercheck_span_error').html("");
                $('#resultofgrammer').hide();
                $('#lblResultsname').hide();
                if (data != null) {
                    $('#resultofgrammer').html("");
                    if (data.hasOwnProperty("error")) {
                        $('#resultofgrammer').hide();
                        $('#lblResultsname').hide();
                        // alert(data.error);
                        $("#spn_Grammar_Spelling_issues").html("0");
                        $("#textgearapiscores").html("0");
                        $("#grammercheck_span_GrammarErorrosTextgearAPI").html("0");
                        return false;
                    } else if (data.hasOwnProperty("errors")) {
                        var totalbetterResults = 0;
                        var betterResults = "";
                        var errorResult = "";
                        var htm1111 = "";
                        var cntid = 0;
                        if ($("#textarea").find('span').length > 0) {
                            $("#resultofgrammer span").contents().unwrap();
                        }
                        // data = sortJSON(data, "offset");
                        //var data1 = sortJSON(data.errors, "offset"); //sort json desc
                        var data1 = data.errors;
                        var counter = 1;
                        for (var i = 0; i < data1.length; i++) {
                            if (data1[i].bad != null && data1[i].bad != "null" && data1[i].bad != " ." && data1[i].bad != ".")
                            {
                                if (errorResult != "") {
                                    errorResult += '\n' + data1[i].bad;

                                } else {
                                    errorResult += data1[i].bad;

                                }
                                if (htm1111 != "") {
                                    htm1111 += '<div class="mainerrorstart">'
                                } else {
                                    htm1111 = '<div class="mainerrorstart">'
                                }
                                var htmlsuggetionssections = "No more suggetions for this word/sentence.";
                                var htmlonlywordssuggetionssections = "No more suggetions for this word/sentence.";
                                var htmlseggetionsstr = "";
                                var htmlonlywordseggetionsstr = "";
                                if (htm1111 != "") {
                                    htm1111 += '<div class="mainerror" id="' + cntid + '"><p class="seqnum">' + counter + ')</p><div  class="lefterror">' +
                                            '<p class="red" data-sugt="' + cntid + '" data-length="' + data1[i].length + '"  data-offset="' + data1[i].offset + '">' + data1[i].bad + '</p><span>→</span></div><div class="errorcorrection"><span class="green" data-sugt="' + cntid + '">';
                                    htmlseggetionsstr += '<div class="mainerror" id="' + cntid + '"><div  class="lefterror">' +
                                            '<p class="red" data-sugt="' + cntid + '" data-length="' + data1[i].length + '"  data-offset="' + data1[i].offset + '">' + data1[i].bad + '</p><span>→</span></div><div class="errorcorrection"><span class="green" data-sugt="' + cntid + '">';
                                    htmlonlywordseggetionsstr += '<div class="mainerror" id="' + cntid + '"><div  class="lefterror">' +
                                            '<p class="red" data-sugt="' + cntid + '" data-length="' + data1[i].length + '"  data-offset="' + data1[i].offset + '"></p><span style="display:none;">→</span></div><div class="errorcorrection"><span class="green" data-sugt="' + cntid + '">';
                                } else {
                                    htm1111 = '<div class="mainerror" id="' + cntid + '"><p class="seqnum">' + counter + ')</p><div  class="lefterror">' +
                                            '<p class="red" data-sugt="' + cntid + '" data-length="' + data1[i].length + '"  data-offset="' + data1[i].offset + '">' + data1[i].bad + '</p><span>→</span></div><div class="errorcorrection"><span class="green" data-sugt="' + cntid + '">';

                                    htmlseggetionsstr = '<div class="mainerror" id="' + cntid + '"><div  class="lefterror" >' +
                                            '<p class="red" data-sugt="' + cntid + '" data-length="' + data1[i].length + '"  data-offset="' + data1[i].offset + '">' + data1[i].bad + '</p><span>→</span></div><div class="errorcorrection"><span class="green" data-sugt="' + cntid + '">';
                                    htmlonlywordseggetionsstr = '<div class="mainerror" id="' + cntid + '"><div  class="lefterror">' +
                                            '<p class="red" data-sugt="' + cntid + '" data-length="' + data1[i].length + '"  data-offset="' + data1[i].offset + '"></p><span style="display:none;">→</span></div><div class="errorcorrection"><span class="green" data-sugt="' + cntid + '">';
                                }
                                var strerror = data1[i].bad;
                                totalbetterResults = parseInt(totalbetterResults) + data1[i].better.length;

                                counter = parseInt(counter) + 1;


                                var strreplcaed = findAndReplacegrammar($("#textarea").html(), strerror, "<span class='grammarerrorcheck'>" + strerror + "</span>");
                                if (strreplcaed != "") {
                                    $("#textarea").html(strreplcaed);
                                }


                                var newlinecount = 0;

                                if (data1[i].better.length > 1) {
                                    htmlsuggetionssections = htmlseggetionsstr;
                                    htmlonlywordssuggetionssections = htmlonlywordseggetionsstr;
                                }
                                for (var j = 0; j < data1[i].better.length; j++) {


                                    if (j == 0) {

                                        var strparm = escape(strerror + "~" + data1[i].better[j] + "~" + cntid);
                                        var lnk = '@Url.Action("replace", "replace")?str=' + strparm;

                                        htm1111 += "<a href='javascript:' id='a_" + encodeURI(strerror) + "'  " + "  onclick='callreplaceall(\"" + strparm + "\" );' >";
                                        if (betterResults != "") {
                                            if (newlinecount == 0) {

                                                htm1111 += data1[i].better[j];
                                            } else {
                                                htm1111 += "<br/>" + data1[i].better[j];
                                            }
                                            betterResults += '\n' + data1[i].better[j];

                                        } else {
                                            htm1111 += data1[i].better[j];
                                            betterResults += data1[i].better[j];

                                        }
                                        htm1111 += "</a>";
                                    } else {

                                        if (j <= 10) {

                                            var strparm = escape(strerror + "~" + data1[i].better[j] + "~" + cntid);
                                            var lnk = '@Url.Action("replace", "replace")?str=' + strparm;

                                            htmlsuggetionssections += "<a href='javascript:' id='a_" + encodeURI(strerror) + "'  " + "  onclick='callreplaceall(\"" + strparm + "\" );' style='margin-top: -18px;' >";
                                            htmlonlywordssuggetionssections += "<a href='javascript:' id='a_" + encodeURI(strerror) + "'  " + "  onclick='callreplaceall(\"" + strparm + "\" );' style='margin-top: -18px;' >";
                                            if (betterResults != "") {
                                                if (newlinecount == 0) {

                                                    htmlsuggetionssections += data1[i].better[j];
                                                    htmlonlywordssuggetionssections += data1[i].better[j];
                                                } else {
                                                    htmlsuggetionssections += "<br/>" + data1[i].better[j];
                                                    htmlonlywordssuggetionssections += "<br/>" + data1[i].better[j];
                                                }
                                                betterResults += '\n' + data1[i].better[j];

                                            } else {
                                                htmlsuggetionssections += data1[i].better[j];
                                                htmlonlywordssuggetionssections += data1[i].better[j];
                                                betterResults += data1[i].better[j];

                                            }
                                            htmlsuggetionssections += "</a>";
                                            htmlonlywordssuggetionssections += "</a>";
                                        }
                                    }
                                    newlinecount = parseInt(newlinecount) + 1;
                                }

                                $('#grammercheck_span').html(totalbetterResults + " Results Found. \n \n" + betterResults);
                                newlinecount = 0;
                                //$('#grammercheck_span_error').html(totalbetterResults + " Results Found. \n \n" + errorResult); //commented 8-oct-2016
                                $('#grammercheck_span_error').html(data.errors.length + " Errors Found. <br/>" + errorResult);
                                //$("#loading1").css("display", "none");
                                htm1111 += '</span></div></div>';

                                htm1111 += '<div class="opensuggetswrds" id="firstopensuggetswrds_' + cntid + '"><a href="javascript:" class="lnkopensuggetswrds" data-errid="' + cntid + '"  id="lnkopensuggetswrds_' + cntid + '"><img src="../Images/card-open.png"/></a><div class="closemainerror" id="closemainerror_' + cntid + '"><a href="javascript:" class="lnkundoclose" data-errid="' + cntid + '" id="lnkundoclose_' + cntid + '"><img src="../Images/close-section.png"/></a></div><div class="undodiv" id="undodiv_' + cntid + '" style="display:none;" data-errid="' + cntid + '"><a href="javascript:" class="lnkundoopen"  id="lnkundoopen_' + cntid + '">Undo</a></div>';
                                if (htmlsuggetionssections.trim() != "No more suggetions for this word/sentence.") {
                                    htm1111 += '<div id="opensuggetswrds_' + cntid + '" style="display:none;" class="divopensuggetswrds"><a href="javascript:" class="lnkclosesuggetionspopup" data-errid="' + cntid + '" id="lnkclosesuggetionspopup_' + cntid + '"><img src="../Images/card-close.png"/></a><a href="javascript:" class="undodivpopup" data-errid="' + cntid + '" id="undodivpopup_' + cntid + '"><img src="../Images/close-section.png"/></a>' + htmlsuggetionssections + '</span></div></div></div>';
                                } else {
                                    htm1111 += '<div id="opensuggetswrds_' + cntid + '" style="display:none;" class="divopensuggetswrds"><a href="javascript:" class="lnkclosesuggetionspopup" data-errid="' + cntid + '" id="lnkclosesuggetionspopup_' + cntid + '"><img src="../Images/card-close.png"/></a><a href="javascript:" class="undodivpopup" data-errid="' + cntid + '" id="undodivpopup_' + cntid + '"><img src="../Images/close-section.png"/></a>' + htmlsuggetionssections + '</div>';
                                }
                                htm1111 += '<div id="onlyopensuggetswrds_' + cntid + '" style="display:none;" class="onlyopensuggetswrds">' + htmlonlywordssuggetionssections + '</span></div></div></div>';
                                htm1111 += '</div>'
                                cntid = parseInt(cntid) + 1;
                                $("#spn_Grammar_Spelling_issues").html(data.errors.length);
                            }
                            if (data.errors.length > 0) {

                                $("#textgearapiscores").html(data.score);
                                $("#grammercheck_span_GrammarErorrosTextgearAPI").html(data.score);
                            }

                        }

                        $('#lblResultsname').show();
                        $('#resultofgrammer').show();

                        $('#resultofgrammer').append(htm1111);
                        characters_sentence_word_count();
                        //var editableDiv = document.getElementById("textarea");
                        // cursorManager.setEndOfContenteditable(editableDiv);
                        //  characters_sentence_word_count();
                        // $("#loading1").css("display", "none");
                    } else {
                        characters_sentence_word_count();
                        $('#grammercheck_span').html("No Results !");
                        $('#grammercheck_span_error').html("No errors !");

                        $("#spn_Grammar_Spelling_issues").html("0");
                        $("#textgearapiscores").html("0");
                        $("#grammercheck_span_GrammarErorrosTextgearAPI").html("0");

                        $('#resultofgrammer').hide();
                        $('#lblResultsname').hide();
                        //$("#loading1").css("display", "none");
                    }

                }

            },
            error: function (ts) { //$("#loading1").css("display", "none"); 
                alert("Text gear api throws an error ! Please try again.");
                return false;
            }

        });
    }
    $(window).load(function () {
        if ($("#textarea").text().trim() != "") {

            //   placeCaretAtEnd(textarea);
            characters_sentence_word_count();

            // grammarCheck();
            //$("#pkessayid").val($("#indessaypkid").val());
        } else {
            $("#textarea").text($("#textarea").text().trim());
            // placeCaretAtEnd(textarea);
            characters_sentence_word_count();
        }

    });
    //function for place focus at end and 
    function placeCaretAtEnd(el) {

        el.focus();

        //
        //if (typeof window.getSelection != "undefined"
        //        && typeof document.createRange != "undefined") {
        //    var range = document.createRange();
        //    range.selectNodeContents(el);
        //    range.collapse(false);
        //    var sel = window.getSelection();
        //    sel.removeAllRanges();
        //    sel.addRange(range);
        //} else if (typeof document.body.createTextRange != "undefined") {
        //    var textRange = document.body.createTextRange();
        //    textRange.moveToElementText(el);
        //    textRange.collapse(false);
        //    textRange.select();
        //}

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
        var browserIsIE = false;
        if (msie > 0) // If Internet Explorer, return version number
        {
            browserIsIE = true;
            // alert(parseInt(ua.substring(msie + 5, ua.indexOf(".", msie))));
        } else  // If another browser, return 0
        {
            //alert('otherbrowser');
        }


        if (browserIsIE) {
            var range = el.createTextRange();
            range.collapse(false);
            range.select();
        } else {
            el.focus();
            var v = el.value;
            el.value = '';
            el.value = v;

        }

    }

    //Namespace management idea from http://enterprisejquery.com/2010/10/how-good-c-habits-can-encourage-bad-javascript-habits-part-1/
    (function (cursorManager) {

        //From: http://www.w3.org/TR/html-markup/syntax.html#syntax-elements
        var voidNodeTags = ['AREA', 'BASE', 'BR', 'COL', 'EMBED', 'HR', 'IMG', 'INPUT', 'KEYGEN', 'LINK', 'MENUITEM', 'META', 'PARAM', 'SOURCE', 'TRACK', 'WBR', 'BASEFONT', 'BGSOUND', 'FRAME', 'ISINDEX'];

        //From: http://stackoverflow.com/questions/237104/array-containsobj-in-javascript
        Array.prototype.contains = function (obj) {
            var i = this.length;
            while (i--) {
                if (this[i] === obj) {
                    return true;
                }
            }
            return false;
        }

        //Basic idea from: http://stackoverflow.com/questions/19790442/test-if-an-element-can-contain-text
        function canContainText(node) {
            if (node.nodeType == 1) { //is an element node
                return !voidNodeTags.contains(node.nodeName);
            } else { //is not an element node
                return false;
            }
        }
        ;

        function getLastChildElement(el) {
            var lc = el.lastChild;
            while (lc && lc.nodeType != 1) {
                if (lc.previousSibling)
                    lc = lc.previousSibling;
                else
                    break;
            }
            return lc;
        }

        //Based on Nico Burns's answer
        cursorManager.setEndOfContenteditable = function (contentEditableElement) {

            while (getLastChildElement(contentEditableElement) &&
                    canContainText(getLastChildElement(contentEditableElement))) {
                contentEditableElement = getLastChildElement(contentEditableElement);
            }

            var range, selection;
            if (document.createRange)//Firefox, Chrome, Opera, Safari, IE 9+
            {
                range = document.createRange();//Create a range (a range is a like the selection but invisible)
                range.selectNodeContents(contentEditableElement);//Select the entire contents of the element with the range
                range.collapse(false);//collapse the range to the end point. false means collapse to end rather than the start
                selection = window.getSelection();//get the selection object (allows you to change selection)
                selection.removeAllRanges();//remove any selections already made
                selection.addRange(range);//make the range you have just created the visible selection
            } else if (document.selection)//IE 8 and lower
            {
                range = document.body.createTextRange();//Create a range (a range is a like the selection but invisible)
                range.moveToElementText(contentEditableElement);//Select the entire contents of the element with the range
                range.collapse(false);//collapse the range to the end point. false means collapse to end rather than the start
                range.select();//Select the range (make it the visible selection
            }
        }

    }(window.cursorManager = window.cursorManager || {}));

    //end function for place focus at end and 
</script>
<?php get_footer(); ?>
