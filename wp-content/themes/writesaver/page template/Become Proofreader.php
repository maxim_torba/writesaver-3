<?php
/*
 * Template Name: Become a Proofreader
 */

get_header();
if (is_user_logged_in()):
    echo '<script>window.location.href="' . get_site_url() . '"</script>';
exit;
endif;
?>
<section>
    <div class="breadcum">
        <div class="container">
            <div class="page_title">
                <?php the_title('<h1>', '</h1>'); ?>
            </div>
        </div>
    </div>
</section>
<section>
    <?php
    if (have_posts()) :
        while (have_posts()) : the_post();
            the_content();
        endwhile;
    endif;
    ?>

</section>
<?php get_footer(); ?>     
