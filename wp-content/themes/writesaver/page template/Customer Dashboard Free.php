<?php

/*
 * Template Name: Customer Dashboard Free
 */
get_header();
$current_user = wp_get_current_user();
$user_roles_array = $current_user->roles;
$user_role = array_shift($user_roles_array);

if (!is_user_logged_in() || $user_role != "customer") {
    echo '<script>window.location.href="' . get_site_url() . '"</script>';
    exit;
}

global $wpdb;
$user_id = get_current_user_id();
$result_free = $wpdb->get_results("SELECT * FROM tbl_customer_general_info WHERE fk_customer_id= $user_id ");
?>

<section>
    <div class="breadcum">
        <div class="container">
            <div class="page_title">
                <h1>Dashboad Test</h1>
            </div>
        </div>
    </div>
</section>

<?php

$desc = "Let me explain you in short, what is the project is about. This Website will be a Real time Editing Website (take an Eg. of Google doc, where we can write/edit in realtime) So this is a same concept. Here there will be two accounts, one for Customer (User) and another is of Proofreader (Freelancer). Customer will upload the document or he can write it in an editing tool area and then he can submit it for proofreading. (Proofreading is a thing where the proofreaders can check the customer's document or whatever they have written and they correct grammatical mistakes and they again submit it back to the customers and then customers pays to the proofreaders) Once they submit it, the customers will not be able to edit it unless the proofreaders submit it back. Then Proofreaders will go through their writings and they will be able to edit it, whatever the proofreaders edit, it should be tracked, also the editing cursor can be seen by the Customers (take an Eg. of google doc, when two persons are editing or writing then they can see each other cursor in different colors) Customer can register themselves, by choosing any membership plan. Proofreaders can also register, they have to give the test. Customer can pay for the extra words. Proofreaders can get the payment based on the words they have edited.  
So this is short concept of this Website, Please read the full detail requirement which explains all the details very clearly below.  
Client has also provided the Wireframes just to get an idea/logic. So while you read it also go through their wireframes. Please DO NOT consider the wireframe for the design, we need to think and we need to design the pages, so feel free to use your creativity.  
Wireframes are just to give you an idea regarding his requirements so you cannot confuse. He has also provided a short video of editing tool, also go through it to understand the flow of the editing tool. Like for customers, this page will have a popout for profile and settings, with a payment page (see the upwork payment page for inspiration) o The payment page will show them how many words they have edited and how much money they have earned, and let them choose their payment method and update their payment profiles and settings. o There will be a page to share a personal link, which will give their friends a free 2000 words after subscribing.   
Blog Main Page This will be a page showing all recent blog postings and content from Writesaver. (for inspiration: http://blog.resolutionmarketing.com.au/) There are a lot of good blog main pages online, and this can be one of the last things we do. There should be good templates available for this, no need to invent something totally new.   
Blog Post Pages These will be the individual pages for blog posts. Again, no need to create something new, a good template will work fine. We can find good sites for inspiration later, and come to a mutual decision on how it should look.  
There should be a separate blog post page template for video content. We should also have templates ready for things like infographics, if possible.  
The Editing Tool/App This is the most important part of this project, and will be the key piece of the website. The layout and design must be clean and modern, and the user interface must be very user friendly, especially on the proofreaders’ side. There will be 2 versions of this tool: the customer side (the ones who submit their writing for review), and the proofreader side (the ones who edit). While the interfaces will look similar, functionally they will be different.  
(Inspiration: http://grammarly.com/, after registering and creating a new document. Evernote (I really like their clean, simple layout for the word processor).   
Customer Interface This will be the first thing customers see, both on the homepage AND on the customer profile page. As such, it has to look amazing.   
On the Home Page: User will be able to enter text and press a button Edit now!, but this will only prompt a registration, which take them to the Customer profile page.
";

echo $desc . '<br><br>';

$totalNoOfWords = str_word_count($desc);
$max_words_limit = 300;
$minus_words_limit = 50;

$remaing_word = $totalNoOfWords % $max_words_limit;
$num_of_chunk = $totalNoOfWords / $max_words_limit;
$num_of_chunk = floor($num_of_chunk);

echo 'totalNoOfWords = ' . $totalNoOfWords . ',<br> remaing_word = ' . $remaing_word . ',<br> num_of_chunk = ' . $num_of_chunk . ',<br><br> max_words_limit = ' . $max_words_limit . ',<br> minus_words_limit = ' . $minus_words_limit;


$start = 0;
for ($i = 1; $i <= $num_of_chunk; $i++) {
    
    
    $chunk_string = implode(' ', array_slice(explode(' ', $desc), $start, $max_words_limit));
    echo '<br><br>'. $i . ') start = ' . $start. ', max_words_limit = ' . $max_words_limit;
    echo ') <br>' . $chunk_string;
    
    $start = $max_words_limit++;
    $max_words_limit = $max_words_limit + $max_words_limit;
    
}
?>

<?php get_footer(); ?>