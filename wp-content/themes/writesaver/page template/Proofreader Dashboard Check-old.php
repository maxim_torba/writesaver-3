<?php
/*
 * Template Name: Proofreader Dashboard Check old
 */

get_header();

$current_user = wp_get_current_user();
$user_roles_array = $current_user->roles;
$user_role = array_shift($user_roles_array);
if (!is_user_logged_in() || $user_role != "proofreader") {
    echo '<script>window.location.href="' . get_site_url() . '"</script>';
    exit;
}


global $wpdb;
$datetime = date('Y-m-d H:i:s');
$user_id = get_current_user_id();


$result = $wpdb->get_results("SELECT * FROM wp_assigned_document_details WHERE fk_proofreader_id= $user_id AND status='In Process' ORDER BY pk_assigned_id DESC LIMIT 1 ");
$assinged_proofreaderid = $result[0]->fk_proofreader_id;
$assinged_doc_proofreader_name = $wpdb->get_results("SELECT display_name from wp_users where ID= $assinged_proofreaderid ");

//get other docs
//$resultothersdocs = $wpdb->get_results("SELECT * FROM wp_assigned_document_details INNER JOIN wp_customer_document_details on "
//        . "wp_assigned_document_details.fk_doc_details_id=wp_customer_document_details.pk_doc_details_id WHERE"
//        . " wp_assigned_document_details.fk_proofreader_id != $user_id AND wp_assigned_document_details.status='In Process'");
//print_r($resultothersdocs);
$resultothersdocs = $wpdb->get_results("SELECT * FROM `wp_customer_document_details` join wp_assigned_document_details on"
        . " wp_customer_document_details.pk_doc_details_id = wp_assigned_document_details.fk_doc_details_id where "
        . "wp_assigned_document_details.fk_proofreader_id=$user_id and wp_assigned_document_details.status='In Process'");




if (count($result) > 0) {
    $doc_details_id = $result[0]->fk_doc_details_id;
    $result_doc = $wpdb->get_results("SELECT * FROM wp_customer_document_details WHERE pk_doc_details_id= $doc_details_id ");


    $status = $result_doc[0]->status;

    $cust_id = $result_doc[0]->fk_cust_id;
    $proof_id = $user_id;
    $desc = "We've started editing your document!";
    if ($status == "pending") {

        echo send_cust_notification($cust_id, $proof_id, $desc);
    }
} else {
   

    $result_doc = $wpdb->get_results(" SELECT * FROM wp_customer_document_details where status='pending' and is_active=1 LIMIT 1 ");
    $status = $result_doc[0]->status;

    $cust_id = $result_doc[0]->fk_cust_id;
    $proof_id = $user_id;
    $desc = "We've started editing your document!";
    if ($status == "Pending") {
        echo send_cust_notification($cust_id, $proof_id, $desc);
    }

    $cust_info = get_userdata($cust_id);
    $proof_info = get_userdata($proof_id);

    $to = $cust_info->user_email;
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $headers .= 'From: Writesaver' . "\r\n";
    wp_mail('ankita.vekariya@aliansoftware.com', $desc, $desc, $headers);

    global $wpdb;
    $wpdb->insert(
            'tbl_customer_notifications', array(
        'notification_date' => date('Y-m-d H:i:s'),
        'fk_customer_id' => $cust_id,
        'fk_proofreader_id' => $proof_id,
        'description' => $desc
            )
    );
    die();
    $wpdb->update(
            'wp_customer_document_details', array(
        'status' => 'In Process',
        'modified_date' => $datetime
            ), array(
        'pk_doc_details_id' => $result_doc[0]->pk_doc_details_id)
    );

    $wpdb->insert(
            'wp_assigned_document_details', array(
        'fk_doc_details_id' => $result_doc[0]->pk_doc_details_id,
        'fk_doc_main_id' => $result_doc[0]->fk_doc_main_id,
        'fk_cust_id' => $result_doc[0]->fk_cust_id,
        'fk_proofreader_id' => $user_id,
        'assign_date' => $datetime,
        'status' => 'In Process',
        'created_date' => $datetime
            )
    );
}




$usersDtl = array(226, "proofreader");
array_push($usersDtl, 225, "customer");
?>
<section>
    <div class="breadcum">
        <div class="container">
            <div class="page_title">
                <h1>Dashboard</h1>
            </div>
        </div>
    </div>
</section>
<section class="proof privacy">
    <div class="container">
        <div class="doc_name">
            <h2>Document-<?php echo $result[0]->fk_doc_details_id; ?></h2>
        </div>
        <div class="timer_main">
            <div class="place"> H : M : S</div>
            <div id="getting-started"></div>
        </div>
        <div class="main_editor">
            <form id="frmSubmitedDoc" name="frmSubmitedDoc">
                <div class="editor_top">
                    <div class="editor_inner_top">                            
                        <div class="used_word">
                            <span>Word Count:</span>&nbsp;<span class="count"><?php echo str_word_count($result_doc[0]->document_desc); ?></span>
                        </div>
                    </div>
                    <input type="hidden" id="hdnProofreaderName" name="hdnProofreaderName" value="<?php echo $assinged_doc_proofreader_name[0]->display_name; ?>">
                    <div class="hidden_scroll check">
                        <div class="changeable check" contenteditable="true" id="txt_area_upload_doc" data-id="<?php echo $result_doc[0]->pk_doc_details_id; ?>" style="width: 100%; display: inline-block;white-space: pre-line;" >
                            <p data-id="<?php echo $result_doc[0]->pk_doc_details_id; ?>"> <?php echo trim($result_doc[0]->document_desc); ?>  </p>
                        </div>
                        <div class="changeable check" id="otherproofdocs">

                        </div>

                        <div id="original" style="white-space: pre-line;position: relative;display:none;height:500px; width: 100%;white-space: pre-line; display:none;">
                            <?php echo trim($result_doc[0]->document_desc); ?>
                        </div>
                        <div id="temdoc" style="white-space: pre-line;position: relative;display:none;"><?php echo trim($result_doc[0]->document_desc); ?>
                        </div>
                        <div id="deletedwords" style="display: none;"></div>
                        <div id="deletedwordsbackspace" style="display: none;"></div>
                        <input type="hidden" id="hdndocidpartsid" name="hdndocidpartsid" value="<?php echo $result_doc[0]->pk_doc_details_id; ?>" />
                        <input type="hidden" id="fk_proofreader_id" name="fk_proofreader_id" value="<?php echo $assinged_proofreaderid ?>" />
                    </div>
                </div>
                <div class="submit_area">
                    <div class="btn_blue">
                        <a href="javascript:void(0);" class="btn_sky" id="btnFinishedMySection">Finished my section</a>
                        <input type="hidden" id="hdnMainDocId" name="hdnMainDocId" value="<?php echo $result_doc[0]->fk_doc_main_id; ?>">
                        <input type="hidden" id="hdnSubDocId" name="hdnSubDocId" value="<?php echo $result_doc[0]->pk_doc_details_id; ?>">
                        <input type="hidden" id="hdnCustomerId" name="hdnCustomerId" value="<?php echo $result_doc[0]->fk_cust_id; ?>">
                    </div>
                </div>
            </form>
        </div>
        <div class="row service">
            <div class="col-sm-4">
                <div class="total_ammount">
                    <div class="left">
                        <h4>83<span>Docs.</span></h4>
                        <div class="divider"></div>
                        <p>Total <br>Docs. worked on</p>
                    </div>
                    <div class="right"></div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="total_ammount paid">
                    <div class="left">
                        <h4>20000<span>Words</span></h4>
                        <div class="divider"></div>
                        <p>Total<br> words edited</p>
                    </div>
                    <div class="right"></div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="total_ammount remaining">
                    <div class="left">
                        <h4>$500<span>Money</span></h4>
                        <div class="divider"></div>
                        <p>Total<br> earnings</p>
                    </div>
                    <div class="right"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="proof_academy">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2>Proofreader academy</h2>
            </div>
            <div class="col-sm-8">
                <div id="accordion" class="ui-accordion ui-widget ui-helper-reset" role="tablist">
                    <h3 class="ui-accordion-header ui-corner-top ui-state-default ui-accordion-icons ui-accordion-header-collapsed ui-corner-all" role="tab" id="ui-id-1" aria-controls="ui-id-2" aria-selected="false" aria-expanded="false" tabindex="0"><span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e"></span>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h3>
                    <div class="ui-accordion-content ui-corner-bottom ui-helper-reset ui-widget-content" id="ui-id-2" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="true" style="display: none;">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                        <p> It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    </div>
                    <h3 class="ui-accordion-header ui-corner-top ui-accordion-header-collapsed ui-corner-all ui-state-default ui-accordion-icons" role="tab" id="ui-id-3" aria-controls="ui-id-4" aria-selected="false" aria-expanded="false" tabindex="-1"><span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e"></span>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h3>
                    <div class="ui-accordion-content ui-corner-bottom ui-helper-reset ui-widget-content" id="ui-id-4" aria-labelledby="ui-id-3" role="tabpanel" aria-hidden="true" style="display: none;">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                        <p> It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    </div>
                    <h3 class="ui-accordion-header ui-corner-top ui-accordion-header-collapsed ui-corner-all ui-state-default ui-accordion-icons" role="tab" id="ui-id-5" aria-controls="ui-id-6" aria-selected="false" aria-expanded="false" tabindex="-1"><span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e"></span>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h3>
                    <div class="ui-accordion-content ui-corner-bottom ui-helper-reset ui-widget-content" id="ui-id-6" aria-labelledby="ui-id-5" role="tabpanel" aria-hidden="true" style="display: none;">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                        <p> It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>                                      
                    </div>                                    
                </div>
            </div><div class="col-sm-4">
                <div class="inner_connent">
                    <img src="<?php echo get_template_directory_uri() ?>/images/desktop_img.png" alt="image" />
                    <p>
                        How to do accurately and faster proofreading?
                    </p>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="loggedProofReaderId" value="<?php echo get_current_user_id(); ?>">
    <input type="hidden" id="hdnwordcount" value="0" name="hdnwordcount">

    <?php
    foreach ($usersDtl as $value) {
        echo '<input type="hidden" name="hdnarrayofUsers[]" value="' . $value . '">';
    }
    ?>
</section>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js" type="text/javascript"></script>-->
<!--<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>-->   
<script src="<?php echo get_template_directory_uri() ?>/js/node_modules/socket.io-client/socket.io.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/nodeClient.js"></script>
<?php get_footer(); ?>
<script>


    var lstedited = [];
    var lstoriginal = [];
    var listofwordschanged = [];
    var deletebackspacewords = [];
    var getdeletedWordsfinal = [];
    function getlines()
    {

        var enteredoriginalText = $("#original").html().trim();
        var numberOfLineBreaksoriginal = (enteredoriginalText.match(/\n/g) || []).length;
        var orginal_numbers_line = numberOfLineBreaksoriginal;
        var lines = $("#original").html().trim().split("\n");

        var row = 0;
        $.each(lines, function (n, elem) {
            if (elem != "" && elem != null)
            {
                var colms = 0;
                if (elem.indexOf(' ') > -1)
                {
                    var strbreakwords = elem.split(' ');
                    for (i = 0; i < strbreakwords.length; i++)
                    {
                        colms = i;
                        lstoriginal.push([row, elem.toString().trim(), colms, strbreakwords[i]]);
                    }
                } else
                {
                    //only one word
                    lstoriginal.push([row, elem.toString().trim(), colms, elem.toString()]);
                }
                row++;
            }
        });

    }




    String.prototype.highLightAt = function (index) {
        return  this.substr(index, 1);
    }

    $(window).load(function () {
        getlines();
    });





    function GetOffset(offset, length, currenttext)
    {
        var strcontents = CleardivForNewline($("#temdoc").html().trim());

        if (strcontents.substr(offset, length).indexOf(" ") != -1) {
            var contents = strcontents.substr(offset, length);
            var lastchar = contents.substr(contents.length - 1);
            var firstchar = contents.substr(0, 1);
            if (firstchar == " ") {
                if (length != 1) {
                    offset = parseInt(offset) + 1;
                    newstring = strcontents.substr(offset, length);
                } else {
                    offset = offset;
                    newstring = strcontents.substr(offset, length);
                }
            }
            else if (lastchar == " ") {
                if (length != 1) {
                    offset = parseInt(offset) - 1;
                    newstring = strcontents.substr(offset, length);
                } else {
                    offset = offset;
                    newstring = strcontents.substr(offset, length);
                }
            } else {
                var newstring = strcontents.substr(offset, length);
                while (currenttext.trim() != newstring.trim()) {
                    offset = parseInt(offset) - 1;
                    newstring = strcontents.substr(offset, length);
                }
            }
        } else {
            var newstring = strcontents.substr(offset, length);

            while (currenttext.trim() != newstring.trim()) {
                offset = parseInt(offset) + 1;
                newstring = strcontents.substr(offset, length);
            }

        }
        $("#temdoc").html($('#txt_area_upload_doc').html());
        return  offset + "_" + newstring;
    }
    function CleardivForNewline(originalhtml) {

        var str = originalhtml;

        var stralllines = str.split("\n")
        var finalvalues = "";
        for (var i = 0; i < stralllines.length; i++) {
            if (finalvalues == "") {
                finalvalues = stralllines[i];
            } else {
                if (stralllines[i] == "" || stralllines[i] == null) {
                    finalvalues = finalvalues + "\n " + stralllines[i];
                } else {
                    finalvalues = finalvalues + "\n " + stralllines[i];
                }
            }
        }
        return finalvalues;
    }
    $(document).ready(function () {
        var temparrybackspacechnaged = [];
        $('#txt_area_upload_doc').keydown(function (e) {
            var updatednew = "";
            var userSelection;
            if (window.getSelection) {
                userSelection = window.getSelection();
            }
            var start = userSelection.anchorOffset;
            var end = userSelection.focusOffset;
            var position = [start, end];
            var val = $(this).text();

            //new logic for old words get

            var oldstringnewlogic;
            if (e.keyCode == 8) {
                console.log("back space2");
                //debugger;
                var startpostions = start;
                var endpostions = end;

                var temptext = $("#original").text().trim();

                while (temptext != " ")
                {
                    startpostions = parseInt(startpostions) - 1;
                    temptext = val.substring(startpostions, start);
                    temptext = temptext.substr(0, 1);
                }
                temptext = $("#original").text().trim();

                while (temptext != " ")
                {
                    endpostions = parseInt(endpostions) + 1;
                    temptext = val.substring(end, endpostions);
                    temptext = temptext.substr(temptext.length - 1);
                }

                //alert(val.substring(startpostions, endpostions));
                oldstringnewlogic = val.substring(startpostions, endpostions);
                if (start >= startpostions && end <= endpostions)
                {
                    if (temparrybackspacechnaged.length == 0)
                    {
                        temparrybackspacechnaged.push([startpostions, endpostions, oldstringnewlogic]);
                    }
                    else
                    {
                        for (var kc = 0; kc <= temparrybackspacechnaged.length - 1; kc++)
                        {
                            if (startpostions > temparrybackspacechnaged[kc][0] && endpostions < temparrybackspacechnaged[kc][1])
                            {
                                temparrybackspacechnaged.push([startpostions, endpostions, oldstringnewlogic]);
                                break;
                            }
                        }
                    }
                }


            }
            //end for new logic
            var length = parseInt(end) - parseInt(start);
            if (length == 0)
            {
                length = 1;

            }
            //var getoffsetandstring = GetOffset(start, length, val.substring(position[0], position[1])); //commented

            var offset = start; //chnaged
            var oldstring = ""; //changed

            var deleted = '';
            if (e.keyCode == 8) {
                console.log("back space1");

//                if (position[0] == position[1]) {
//                    if (position[0] == 0)
//                    {
//                        deleted = '';
//                        offset = offset;
//                        length = length;
//                        oldstring = deleted;
//                    }
//                    else
//                    {
//                        deleted = val.substr(parseInt(position[0]) - 1, 1);
//                        offset = parseInt(position[0]) - 1;
//
//                        oldstring = deleted;
//                    }
//                }
//                else {
//                    deleted = val.substring(position[0], position[1]);
//                    offset = position[0];
//                }
//
//                updatednew = deleted;
//                console.log("backspace call" + offset + " " + length + " " + oldstring + " " + updatednew + " " + "Delete BackSpace");
                oldstring = oldstringnewlogic;
                listofwordschanged.push([offset, length, oldstring, updatednew, "Delete BackSpace"]);
                //comparedocs();

            }
            else if (e.keyCode == 46) {
                debugger;
                var val = $(this).text();
                if (position[0] == position[1]) {
                    if (position[0] === val.length)
                        deleted = '';
                    else
                        deleted = val.substr(position[0], 1);
                }
                else {
                    deleted = val.substring(position[0], position[1]);
                }
                //console.log("deleted words call");
                updatednew = deleted;
                oldstring = deleted;
                offset = position[0];
                // alert(deleted);
                listofwordschanged.push([offset, length, oldstring, updatednew, "Delete"]);
                //comparedocs();

            } else if (e.which !== 0) {
                var c = String.fromCharCode(e.which);
                var isWordCharacter = c.match(/\w/);
                if ((isWordCharacter)) {
                    var val = $(this).text();


                    updatednew = String.fromCharCode(e.which).toLowerCase();
                    oldstring = oldstring;
                    length = 1;
                    offset = position[0];


                    var tmpstartpostions = parseInt(position[0]) - 1;
                    var tmpstartstring = val.highLightAt(tmpstartpostions);

                    var removestart = 0;
                    var preveiousstrings = "";
                    while (tmpstartstring.trim().length > 0)
                    {
                        if (tmpstartstring.trim().length != 0)
                        {
                            tmpstartpostions = parseInt(tmpstartpostions) - 1;
                            preveiousstrings += tmpstartstring;
                        }
                        removestart++;
                        tmpstartstring = val.highLightAt(tmpstartpostions);


                    }


                    oldstring = "";
                    updatednew = "";
                    var tmpendpostions = parseInt(position[0]) - removestart;
                    offset = tmpendpostions - 1;
                    var tmpendstring = val.highLightAt(tmpendpostions);

                    preveiousstrings += tmpendstring;
                    while (tmpendstring.trim().length > 0)
                    {
                        tmpendstring = val.highLightAt(tmpendpostions);
                        // alert("end" + (tmpendstring));

                        tmpendpostions = parseInt(tmpendpostions) + 1;
                        if (tmpendpostions != position[0])
                        {
                            oldstring += tmpendstring;
                            updatednew += tmpendstring;
                        } else {
                            oldstring += tmpendstring;
                            updatednew += tmpendstring + String.fromCharCode(e.which).toLowerCase();
                        }


                    }
                    var endoffset = parseInt(tmpendpostions);
                    // alert(oldstring.length);
                    length = updatednew.length - 1;
                    for (var kc = 0; kc <= temparrybackspacechnaged.length - 1; kc++)
                    {
                        if (start >= temparrybackspacechnaged[kc][0] && end <= temparrybackspacechnaged[kc][1])
                        {
                            oldstringnewlogic = temparrybackspacechnaged[kc][2];
                            break;
                        } else if (start == end)
                        {
                            oldstringnewlogic = temparrybackspacechnaged[kc][2];
                            break;
                        }
                    }
                    temparrybackspacechnaged.push([offset, parseInt(offset) + parseInt(length), oldstringnewlogic]);
                    console.log(start + " end " + end + " changed:" + " " + offset + " " + length + " oldstring " + oldstring + " oldstringnewlogic " + oldstringnewlogic + " " + updatednew + " " + "Changed or Added");
                    listofwordschanged.push([offset, length, oldstring, updatednew, "Changed or Added"]);

                } else {
                    //up and down and other key pressed so don't need to track
                    //alert("notword");
                }
            }
            $("#deletedwords").text(deleted);


        });


    });


    $("#btnFinishedMySection").click(function ()
    {
        var desc = $("#txt_area_upload_doc").text();
        var data = new FormData();

        var fk_cust_id = $("#hdnCustomerId").val();
        var fk_main_doc_id = $("#hdnMainDocId").val();
        var fk_sub_doc_id = $("#hdnSubDocId").val();

        var fk_proofreader_id = $("#fk_proofreader_id").val();
        var docid = $("#hdndocidpartsid").val();

        var listofwordschangedFinal = [];
        var listofwordschangedFinalMain = [];
        var temparray = [];
        var updatedWordsAry = [];
        listofwordschanged.sort(function (a, b) {
            var aVal = parseInt(a[0]) + parseInt(a[1]),
                    bVal = parseInt(b[0]) + parseInt(b[1]);
            return bVal - aVal;
        });
        for (var k = 0; k <= listofwordschanged.length - 1; k++)
        {


            var sourceId = listofwordschanged[k][0];
            var sourceId1 = listofwordschanged[k][1];
            var sourceId2 = listofwordschanged[k][2];
            var item = temparray.filter(function (collect) {
                return collect[0] == sourceId && collect[1] == sourceId1 && collect[2] == sourceId2; //check offset added or not
            });
            if (item.length == 0)
            {

                if (listofwordschanged[k][4].toString().trim() != "Delete BackSpace" && listofwordschanged[k][4].toString().trim() != "Delete")
                {

                    var newUser = [];
                    newUser.push(listofwordschanged[k][0], listofwordschanged[k][1], listofwordschanged[k][2], listofwordschanged[k][3], listofwordschanged[k][4]);
                    // temparray.push(newUser);
                    updatedWordsAry.push(newUser);
                    //console.log("<br/>\n updated :-  " + k + 1 + " " + listofwordschanged[k][0], listofwordschanged[k][1], listofwordschanged[k][2], listofwordschanged[k][3], listofwordschanged[k][4]);

                }
                else if (listofwordschanged[k][4].toString().trim() == "Delete")
                {
                    var newUser = [];
                    newUser.push(listofwordschanged[k][0], listofwordschanged[k][1], listofwordschanged[k][2], listofwordschanged[k][3], listofwordschanged[k][4]);
                    temparray.push(newUser);
                    console.log("<br/>\n Final delete:-  " + k + 1 + " " + listofwordschanged[k][0], listofwordschanged[k][1], listofwordschanged[k][2], listofwordschanged[k][3], listofwordschanged[k][4]);
                }
            }
        }

        var item = listofwordschanged.filter(function (collect) {
            return collect[4] == "Delete BackSpace";
        });
        if (item.length > 0)
        {
            for (var k = 0; k <= item.length - 1; k++)
            {
                console.log("<br/>\n DelBackSpace  :-  " + k + 1 + " " + item[k][0], item[k][1], item[k][2], item[k][3], item[k][4]);

            }

        }

        var tmpupdatedWordsAry = [];
        tmpupdatedWordsAry = updatedWordsAry;
        var finalupdatedword = [];
        if (tmpupdatedWordsAry.length > 0 && updatedWordsAry.length > 0)
        {
            tmpupdatedWordsAry.sort(function (a, b) {
                var aVal = parseInt(a[0]) + parseInt(a[1]),
                        bVal = parseInt(b[0]) + parseInt(b[1]);
                return aVal - bVal;
            });
            for (var k = 0; k <= updatedWordsAry.length - 1; k++)
            {
                var sourceId1 = updatedWordsAry[k][0];
                var item1 = finalupdatedword.filter(function (collect) {
                    return collect[0] == sourceId1;
                });
                if (item1.length == 0)
                {
                    var sourceId = updatedWordsAry[k][0];
                    var item = tmpupdatedWordsAry.filter(function (collect) {
                        return collect[0] == sourceId; //check offset added or not
                    });
                    var oldstring = updatedWordsAry[k][2];
                    if (item.length > 0)
                    {
                        oldstring = item[0][2];

                    }
                    var id = parseInt(k) + 1;
                    var newUser = [];
                    newUser.push(updatedWordsAry[k][0], updatedWordsAry[k][1], oldstring, updatedWordsAry[k][3], updatedWordsAry[k][4]);
                    finalupdatedword.push(newUser);

                    console.log("<br/>\n updated :-  " + id + " " + updatedWordsAry[k][0], updatedWordsAry[k][1], oldstring, updatedWordsAry[k][3], updatedWordsAry[k][4]);
                }
            }
        }

        return;






        if (desc != "")
        {
            $.ajax({
                type: 'POST',
                url: '<?php echo admin_url('admin-ajax.php'); ?>',
                method: "post",
//                cache: false,
//                contentType: false,
//                processData: false,
                //data: data,
                data: {
                    action: 'submited_doc_by_proofreader',
                    doc_id: docid,
                    userid: fk_proofreader_id,
                    data2: temparray,
                    word_desc: desc,
                    fk_cust_id: fk_cust_id,
                    fk_main_doc_id: fk_main_doc_id,
                    fk_sub_doc_id: fk_sub_doc_id

                },
                type: 'POST',
                        success: function (data) {

                            if (data == 'error')
                            {
                                alert("Please enter data");
                            }
                            else
                            {
                                alert(data);
                            }
                        },
                error: function (jqXHR, textStatus, errorThrown) {
                    //   document.getElementById("txt_area_upload_doc").contentEditable = true;
                    alert(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                }
            });
            return false;
        }
        else
        {
            alert('no data found');
        }
    });
</script>