<?php
/*
 * Template Name: Proofreader Dashboard Check
 */
get_header();
$current_user = wp_get_current_user();
$user_roles_array = $current_user->roles;
$user_role = array_shift($user_roles_array);
if (!is_user_logged_in() || $user_role != "proofreader") {
    echo '<script>window.location.href="' . get_site_url() . '"</script>';
    exit;
}

global $wpdb;
$datetime = date('Y-m-d H:i:s');
$user_id = get_current_user_id();

$info = get_user_meta($user_id, 'info_completed', true);
$test = get_user_meta($user_id, 'test_completed', true);
$test_status = get_user_meta($user_id, 'test_status', TRUE);

if ($info != 1 || $test != 1 || $test_status != 'accepted') {
    echo '<script>window.location.href="' . get_permalink(810) . '"</script>';
    exit;
}

$result = $wpdb->get_results("SELECT * FROM wp_assigned_document_details as A INNER JOIN wp_customer_document_main AS M  ON M.pk_document_id = A.fk_doc_main_id WHERE A.fk_proofreader_id= $user_id AND A.status='In Process' AND M.Status=1 ORDER BY A.pk_assigned_id DESC LIMIT 1 ");
$assinged_proofreaderid = $result[0]->fk_proofreader_id;
$assinged_doc_proofreader_name = $wpdb->get_results("SELECT display_name from wp_users where ID= $assinged_proofreaderid ");


if (count($result) > 0) {


    $doc_details_id = $result[0]->fk_doc_details_id;
    $doc_main_id = $result[0]->fk_doc_main_id;
    $result_doc = $wpdb->get_results("SELECT * FROM wp_customer_document_details WHERE pk_doc_details_id= $doc_details_id AND is_active=1");

    $result_maindoc_name = $wpdb->get_var("SELECT document_title FROM wp_customer_document_main WHERE pk_document_id= $doc_main_id AND Status=1");

    $getlinesdocs = $wpdb->get_results("SELECT * FROM tbl_doc_line_details WHERE Fk_main_doc_id= $doc_main_id ");
} else {

    $double_check_doc = $wpdb->get_row("SELECT * FROM tbl_proofreaded_doc_details as P INNER JOIN wp_customer_document_main AS M  ON M.pk_document_id = P.fk_doc_main_id where  P.Fk_DoubleProofReader_Id =$user_id  AND  P.fk_proofreader_id !=$user_id AND P.DoubleCheckStatus != 1 AND M.Status=1");

    if (!$double_check_doc) {
        $double_check_doc = $wpdb->get_row("SELECT * FROM tbl_proofreaded_doc_details as P INNER JOIN wp_customer_document_main AS M  ON M.pk_document_id = P.fk_doc_main_id where P.Fk_DoubleProofReader_Id IS NULL   AND  P.fk_proofreader_id !=$user_id AND P.DoubleCheckStatus != 1 AND M.Status=1");
    }

    if ($double_check_doc) {

        $doc_details_id = $double_check_doc->fk_doc_details_id;
        $pk_proofreaded_doc_Id = $double_check_doc->pk_proofreaded_doc_Id;
        $wpdb->update('tbl_proofreaded_doc_details', array('Fk_DoubleProofReader_Id' => $user_id), array('fk_doc_details_id' => $doc_details_id, 'pk_proofreaded_doc_Id' => $pk_proofreaded_doc_Id));

        $result_doc = $wpdb->get_results(" SELECT * FROM wp_customer_document_details AS D "
                . " INNER JOIN wp_customer_document_main AS M"
                . " ON D.fk_doc_main_id = M.pk_document_id"
                . " where D.pk_doc_details_id= $doc_details_id AND M.Status=1"
                . " LIMIT 1");
    } else {

        $result_doc = $wpdb->get_results(" SELECT * FROM wp_customer_document_details AS D "
                . " INNER JOIN wp_customer_document_main AS M"
                . " ON D.fk_doc_main_id = M.pk_document_id"
                . " where D.status='Pending'"
                . " and D.is_active=1 AND M.Status=1 ORDER BY M.order_no ASC LIMIT 1");
    }
    $status = $result_doc[0]->status;

    $doc_main_id = $result_doc[0]->fk_doc_main_id;
    $result_maindoc_name = $wpdb->get_var("SELECT document_title FROM wp_customer_document_main WHERE pk_document_id=$doc_main_id AND Status=1");


    $cust_id = $result_doc[0]->fk_cust_id;
    $proof_id = $user_id;
    $usersDtl = array($proof_id, "proofreader");
    array_push($usersDtl, $cust_id, "customer");

    $getlinesdocs = $wpdb->get_results("SELECT * FROM tbl_doc_line_details WHERE Fk_main_doc_id= $doc_main_id ");

    if ($status == "Pending") {


        $wpdb->update(
                'wp_customer_document_details', array(
            'status' => 'In Process',
            'modified_date' => $datetime
                ), array(
            'pk_doc_details_id' => $result_doc[0]->pk_doc_details_id)
        );
    }

    if ($status == "Single Check")
        $assign_status = 'Single Check';
    else
        $assign_status = 'In Process';
    if (count($result_doc) > 0) {
        $wpdb->insert(
                'wp_assigned_document_details', array(
            'fk_doc_details_id' => $result_doc[0]->pk_doc_details_id,
            'fk_doc_main_id' => $result_doc[0]->fk_doc_main_id,
            'fk_cust_id' => $result_doc[0]->fk_cust_id,
            'fk_proofreader_id' => $user_id,
            'assign_date' => $datetime,
            'status' => $assign_status,
            'created_date' => $datetime
                )
        );
    }
}

$status = $result_doc[0]->status;
$cust_id = $result_doc[0]->fk_cust_id;
$proof_id = $user_id;

if ($status == "Pending") {
    $total_docs = $wpdb->get_results("SELECT * FROM wp_customer_document_details WHERE fk_doc_main_id= $doc_main_id AND is_active= 1 ORDER BY pk_doc_details_id");
    $count = 0;
    $pending_count = 0;
    $total_count = count($total_docs);
    foreach ($total_docs as $total_doc) {
        $count++;
        if ($total_doc->pk_doc_details_id == $result_doc[0]->pk_doc_details_id) {
            $doc_count = $count;
        }
        if ($total_doc->status == "Pending") {
            $pending_count++;
        }
    }
    $proof_info = get_userdata($proof_id);
    $subject = "[" . get_bloginfo('name') . "] We have started editing of " . $result_maindoc_name . ".";
    $desc = "<p>We have started editing of " . $result_maindoc_name . ".</p>";
    $noti = "We have started editing of " . $result_maindoc_name . ".";


    $result_user = $wpdb->get_results("SELECT * FROM wp_notification_settings WHERE user_id= $cust_id");
    $desk_noti = $result_user[0]->dash_doc_started;
    $email_noti = $result_user[0]->receive_doc_started;
    if ($pending_count == $total_count) {
        echo send_cust_notification($cust_id, $proof_id, $desc, $desk_noti, $email_noti, $subject, $noti);
    }
}
?>

<section>
    <div class="breadcum">
        <div class="container">
            <div class="page_title">
                <h1>Dashboard</h1>
            </div>
        </div>
    </div>
</section>

<section class="proof privacy">
    <div class="container">
        <?php
        if ($result_doc):
            $doc_detail_id = $result_doc[0]->pk_doc_details_id;
            $details_status = $result_doc[0]->status;
            if ($details_status == 'Pending') {
                $single_start_time = $wpdb->get_var("select single_start_time from wp_customer_document_details where pk_doc_details_id = $doc_detail_id");
                if ($single_start_time == '' || $single_start_time == 0)
                    $wpdb->update('wp_customer_document_details', array('single_start_time' => date('Y-m-d H:i:s')), array('pk_doc_details_id' => $doc_detail_id));
            }
            if ($details_status == 'Single Check') {
                $double_start_time = $wpdb->get_var("select double_start_time from wp_customer_document_details where pk_doc_details_id = $doc_detail_id");
                if ($double_start_time == '' || $double_start_time == 0)
                    $wpdb->update('wp_customer_document_details', array('double_start_time' => date('Y-m-d H:i:s')), array('pk_doc_details_id' => $doc_detail_id));
            }
            ?>
            <div class="doc_name">
                <h2><?php echo $result_maindoc_name; ?></h2>
            </div>

            <div class="main_editor">
                <form id="frmSubmitedDoc" name="frmSubmitedDoc">
                    <div class="editor_top">
                        <div class="editor_inner_top">                            
                            <div class="used_word">
                                <span>Word Count:</span>&nbsp;<span class="count">0</span>
                            </div>
                        </div>
                        <input type="hidden" id="hdnProofreaderName" name="hdnProofreaderName" value="<?php echo $assinged_doc_proofreader_name[0]->display_name; ?>">
                        <div class="hidden_scroll check">
                            <div class="parentscrollcontents" style="width: 100%; height:300px; display: inline-block;">

                                <?php
                                $all_doc = $wpdb->get_results("SELECT * FROM wp_customer_document_details WHERE fk_doc_main_id= $doc_main_id  AND is_active=1");
                                $doc_section = 1;
                                foreach ($all_doc as $doc) {
                                    $sub_document_id = $doc->pk_doc_details_id;
                                    if ($doc->status == 'Pending') {
                                        $single_proof = '';
                                        $double_proof = '';
                                    } elseif ($doc->status == 'In Process') {
                                        $assign_doc = $wpdb->get_row("SELECT * FROM `wp_assigned_document_details` where fk_doc_details_id= $sub_document_id");
                                        $single_proof_id = $assign_doc->fk_proofreader_id;
                                        $single_proof_info = get_userdata($single_proof_id);
                                        $single_proof = $single_proof_info->first_name . ' ' . $single_proof_info->last_name;
                                        $double_proof = '';
                                    } else {
                                        $sub_documents = $wpdb->get_results("SELECT * FROM `tbl_proofreaded_doc_details` WHERE fk_doc_details_id= $sub_document_id LIMIT 1");

                                        if (count($sub_documents) > 0) {
                                            $single_proof_id = $sub_documents[0]->fk_proofreader_id;
                                            $single_proof_info = get_userdata($single_proof_id);
                                            $single_proof = $single_proof_info->first_name . ' ' . $single_proof_info->last_name;
                                            $double_proof_id = $sub_documents[0]->Fk_DoubleProofReader_Id;
                                            if ($double_proof_id != NULL) {
                                                $double_proof_info = get_userdata($double_proof_id);
                                                $double_proof = $double_proof_info->first_name . ' ' . $double_proof_info->last_name;
                                            } else
                                                $double_proof = '';
                                        }
                                    }
                                    if ($double_proof && $double_proof != NULL)
                                        $proof_name = $double_proof;
                                    else
                                        $proof_name = $single_proof;
                                    ?>

                                    <div data-proof_name ="<?php echo $proof_name; ?>" class="changeable check  <?php echo ($doc->pk_doc_details_id == $result_doc[0]->pk_doc_details_id ? "" : "not_editable"); ?>" contenteditable="<?php echo ($doc->pk_doc_details_id == $result_doc[0]->pk_doc_details_id ? "true" : "false"); ?>" id="<?php echo ($doc->pk_doc_details_id == $result_doc[0]->pk_doc_details_id ? "txt_area_upload_doc" : "txt_area_upload_doc_" . $doc->pk_doc_details_id); ?>" data-id="<?php echo $doc->pk_doc_details_id; ?>" style="background-color: <?php echo ($doc->pk_doc_details_id == $result_doc[0]->pk_doc_details_id ? "white" : "#f5f5f5"); ?>; " data-section="<?php echo $doc_section; ?>" >
                                        <?php
                                        $status_1 = $doc->status;
                                        if ($status_1 == "Pending" || $status_1 == "pending" || $status_1 == "In Process") {

                                            echo stripslashes(str_replace("\n", "<br>", trim($doc->document_desc)));
                                        } else {
                                            $docdtl_Id = $doc->pk_doc_details_id;
                                            $Proofreaded_doc1 = $wpdb->get_results("SELECT * FROM tbl_proofreaded_doc_details where fk_doc_details_id= $docdtl_Id");
                                            $status_doc = $Proofreaded_doc1[0]->status;
                                            if ($status_doc == 'Completed')
                                                echo stripslashes(str_replace("\n", "<br>", trim($Proofreaded_doc1[0]->double_doc_desc)));
                                            else
                                                echo stripslashes(str_replace("\n", "<br>", trim($Proofreaded_doc1[0]->doc_desc)));
                                        }
                                        ?>
                                    </div>
                                    <?php
                                    $doc_section++;
                                }
                                ?>
                            </div>

                            <div id="original" style="white-space: pre-line;position: relative;display:none;height:500px; width: 100%;white-space: pre-line; display:none;">
                                <?php echo trim($result_doc[0]->document_desc); ?>
                            </div>
                            <div id="temdoc" style="white-space: pre-line;position: relative;display:none;"><?php echo trim($result_doc[0]->document_desc); ?>
                            </div>
                            <div id="deletedwords" style="display: none;"></div>
                            <div id="deletedwordsbackspace" style="display: none;"></div>
                            <input type="hidden" id="hdndocidpartsid" name="hdndocidpartsid" value="<?php echo $result_doc[0]->pk_doc_details_id; ?>" />
                            <input type="hidden" id="fk_proofreader_id" name="fk_proofreader_id" value="<?php echo $assinged_proofreaderid ?>" />
                        </div>
                    </div>
                    <div class="submit_area">
                        <div class="btn_blue">
                            <a href="javascript:void(0);" class="btn_sky" id="btnFinishedMySection" <?php echo ($double_check_doc) ? 'style="display: none;"' : ''; ?>>Finished my section</a>
                            <a href="javascript:void(0);" class="btn_sky" id="btnLooksPerfect" <?php echo ($double_check_doc) ? '' : 'style="display: none;"'; ?> >Looks Perfect!</a>
                            <input type="hidden" id="hdnMainDocId" name="hdnMainDocId" value="<?php echo $result_doc[0]->fk_doc_main_id; ?>">
                            <input type="hidden" id="hdnSubDocId" name="hdnSubDocId" value="<?php echo $result_doc[0]->pk_doc_details_id; ?>">
                            <input type="hidden" id="hdnCustomerId" name="hdnCustomerId" value="<?php echo $result_doc[0]->fk_cust_id; ?>">
                        </div>
                    </div>
                </form>
            </div>
        <?php else: ?>
            <div class="doc_name">
                <h2>No documents are currently available for proofreading.</h2>
            </div>
        <?php endif; ?>

        <div class="row service">
            <?php
            $user_id = get_current_user_id();
            $result = $wpdb->get_results("SELECT * FROM tbl_proofreader_general_details WHERE fk_proofreader_id= $user_id LIMIT 1 ");

            $total_docs_worked = 0;
            $total_words_edited = 0;
            $total_earning = 0;

            if (count($result) > 0) {
                $total_docs_worked = ($result[0]->total_docs_worked == "" || $result[0]->total_docs_worked == null ? 0 : $result[0]->total_docs_worked);
                $total_words_edited = ($result[0]->total_words_edited == "" || $result[0]->total_words_edited == null ? 0 : $result[0]->total_words_edited );
                $total_earning = ($result[0]->total_earning == "" || $result[0]->total_earning == null ? 0 : $result[0]->total_earning);
            }
            ?>
            <div class="col-sm-4">
                <div class="total_ammount">
                    <div class="left">
                        <h4><?php echo $total_docs_worked; ?><span>Docs</span></h4>
                        <div class="divider"></div>
                        <p>Total <br>Docs worked on</p>
                    </div>
                    <div class="right"></div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="total_ammount paid">
                    <div class="left">
                        <h4><?php echo $total_words_edited; ?><span>Words</span></h4>
                        <div class="divider"></div>
                        <p>Total<br> words edited</p>
                    </div>
                    <div class="right"></div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="total_ammount remaining">
                    <div class="left">
                        <h4>$<?php echo number_format((float) $total_earning, 2, '.', ''); ?><span></span></h4>
                        <div class="divider"></div>
                        <p>Total<br> earnings</p>
                    </div>
                    <div class="right"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--POPUP-->

<div id="finish_section" class="pop_overlay open start_tests ready" style="display: none;">
    <div class="pop_main">
        <div class="pop_head">
            <a href="javascript:void(0);"><i class="fa fa-remove" aria-hidden="true"></i></a>
        </div>
        <div class="pop_body">
            <div class="load_overlay pop_loding" >
                <img src="<?php echo get_template_directory_uri(); ?>/images/39.gif"/>
            </div>
            <div class="page_title">
                <h2>Is this document perfect? </h2>
            </div>
            <div class="pop_content">
                <div class="first_test save_doc">
                    <p>Are you sure you're finished proofreading? You will not be able to access this document again.</p>                       
                    <a href="#" class="btn_sky btnyes">Yes</a>
                    <a href="#" class="btn_sky btnno  orange pop_btn">No</a>                       
                </div>
                <div class="doc_msg"></div>
            </div>
        </div>
    </div>
</div>




<div id="look_perfect" class="pop_overlay open start_tests ready" style="display: none;">
    <div class="pop_main">
        <div class="pop_head">
            <a href="javascript:void(0);"><i class="fa fa-remove" aria-hidden="true"></i></a>
        </div>
        <div class="pop_body">
            <div class="load_overlay pop_loding" >
                <img src="<?php echo get_template_directory_uri(); ?>/images/39.gif"/>
            </div>
            <div class="page_title">
                <h2>Is this document perfect? </h2>
            </div>
            <div class="pop_content">
                <div class="first_test save_doc">
                    <p>Are you sure you're finished with your proofread? You will not be able to access this document again.</p>                       
                    <a href="#" class="btn_sky btnyes">Yes</a>
                    <a href="#" class="btn_sky btnno  orange pop_btn">No</a>                       
                </div>
                <div class="doc_msg"></div>
            </div>
        </div>
    </div>
</div>

<div id="pop_start" class="pop_overlay open start_tests" style="display: none;">
    <div class="pop_main">
        <div class="pop_head">
            <a href="javascript:void(0);"><i class="fa fa-remove" aria-hidden="true"></i></a>
        </div>
        <div class="pop_body">
            <div class="page_title">
                <h2>Double check</h2>
            </div>
            <div class="pop_content">
                <div class="first_test">
                    <p>This document has already been proofread by another proofreader. Please double check it for mistakes. After you finish, the final (mistake free) copy will go back to the customer.</p>
                    <a href="javascript:void(0);" class="btn_sky pop_btn letsCheckit">Check it!</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="pop_start_Confirm" class="pop_overlay open start_tests ready" style="display: none;">
    <div class="pop_main">
        <div class="pop_head">
            <a href="javascript:void(0);"><i class="fa fa-remove" aria-hidden="true"></i></a>
        </div>
        <div class="pop_body">
            <div class="page_title">
                <h2>Finished!</h2>
            </div>
            <div class="pop_content">
                <div class="first_test">
                    <!--<p>This document has been completely proofread.</p>-->
                    <a href="<?php echo get_page_link(810); ?>" class="btn_sky orange pop_btn btnReturnToProfile">Return to dashboard</a>
                    <a href="<?php echo get_page_link(852); ?>" class="btn_sky pop_btn btnStartNextSubmission">Start on next submission!</a>                            
                </div>
            </div>
        </div>
    </div>
</div>
<?php
foreach ($usersDtl as $value) {
    echo '<input type="hidden" name="hdnarrayofUsers[]" value="' . $value . '">';
}
?>
<?php
if (have_posts()) :
    while (have_posts()) : the_post();
        the_content();
    endwhile;
endif;
?>
<script src="<?php echo get_template_directory_uri() ?>/js/node_modules/socket.io-client/socket.io.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/nodeClient.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/diff_new.js"></script>
<?php get_footer(); ?>
<script>


    $(document).ready(function () {

        var doc_detail = $('#txt_area_upload_doc').text().trim();
        //doc_detail = doc_detail.replace(/\s+/g, ' ');
        var words = get_doc_word_count(doc_detail);
        $(".used_word .count").text(words);

        var main = $(".parentscrollcontents");
        t = main.offset().top + 25;
        $('.parentscrollcontents').animate({
            scrollTop: $("#txt_area_upload_doc").offset().top - t
        }, 2000);


        $('#txt_area_upload_doc').bind("DOMNodeInserted", function () {
            if ($(this).text().trim() == "")
            {
                $(".used_word .count").text("0");
            } else
            {
                var doc_detail = $('#txt_area_upload_doc').text().trim();
                //doc_detail = doc_detail.replace(/\s+/g, ' ');
                var words = get_doc_word_count(doc_detail);
                $(".used_word .count").text(words);
            }
        });



        $('#txt_area_upload_doc').keyup(function () {
            if ($(this).text().trim() == "")
            {
                $(".used_word .count").text("0");
            } else
            {
                var doc_detail = $('#txt_area_upload_doc').text().trim();
                //doc_detail = doc_detail.replace(/\s+/g, ' ');
                var words = get_doc_word_count(doc_detail);
                $(".used_word .count").text(words);
            }
        });

        $('.pop_head a').click(function () {
            $('#pop_start').fadeOut('slow');
            var url = '<?php echo get_page_link(810); ?>';
            window.location.href = url;

        });

        $('#finish_section .pop_head a .fa-remove,#finish_section .save_doc .btnno').click(function (event) {
            event.stopPropagation();
            $('#finish_section').fadeOut();
            $('#finish_section').removeClass('open');

        });


        $('#look_perfect .pop_head a .fa-remove, #look_perfect .save_doc .btnno').click(function (event) {
            event.stopPropagation();
            $('#look_perfect').fadeOut();
            $('#look_perfect').removeClass('open');
        });

        $('#finish_section .save_doc .btnyes').click(function (e) {
            $('.docmsg').remove();

            var desc = $('#txt_area_upload_doc').html().trim();
            desc = desc.replace(/<br>/g, "\n");
            desc = desc.replace(/<div>/g, "");
            desc = desc.replace(/<\/div>/g, "\n");
            //desc = desc.replace(/\s+/g, ' ');

            var fk_cust_id = $("#hdnCustomerId").val();
            var fk_main_doc_id = $("#hdnMainDocId").val();
            var fk_sub_doc_id = $("#hdnSubDocId").val();
            var docid = $("#hdndocidpartsid").val();
            var changed_wrod_ary = get_diff_str_array(doc_detail, desc);

            if (desc != "")
            {
                $('.pop_loding').show();
                $.ajax({
                    type: 'POST',
                    url: '<?php echo admin_url('admin-ajax.php'); ?>',
                    method: "post",
                    data: {
                        action: 'submited_doc_by_proofreader',
                        doc_id: docid,
                        changed_wrod_ary: changed_wrod_ary,
                        word_desc: desc,
                        fk_cust_id: fk_cust_id,
                        fk_main_doc_id: fk_main_doc_id,
                        fk_sub_doc_id: fk_sub_doc_id
                    },
                    type: 'POST',
                    success: function (data) {

                        if (data == 'error')
                        {
                            $("#btnFinishedMySection").css("display", "");
                            $("#btnLooksPerfect").css("display", "none");
                            $("#txt_area_upload_doc").attr("contenteditable", "true");
                            $("#txt_area_upload_doc").removeClass("proofreaderdesc");
                            $('.pop_loding').hide();
                        } else
                        {
                            $.ajax({
                                type: 'POST',
                                url: '<?php echo admin_url('admin-ajax.php'); ?>',
                                method: "post",
                                data: {
                                    action: 'getDoubleCheckDesc'
                                },
                                type: 'POST',
                                success: function (data1) {

                                    var res = jQuery.parseJSON(data1);
                                    $('#pop_start').css("display", "none");
                                    if (data1.trim() == "0")
                                    {
                                        $(".doc_msg").html('<span class="text-success docmsg">Document submitted successfully...</span>');
                                        $("#btnFinishedMySection").css("display", "");
                                        $("#btnLooksPerfect").css("display", "none");
                                        $("#txt_area_upload_doc").attr("contenteditable", "true");
                                        $('.pop_loding').hide();
                                        window.setTimeout(function () {
                                            // var url = '<?php echo get_page_link(810); ?>';
                                            // window.location.href = url;
                                            $('#finish_section').fadeOut();
                                            $('#finish_section').removeClass('open');
                                            $('#pop_start_Confirm').fadeIn('slow');

                                        }, 1000);
                                    } else
                                    {
                                        $('#finish_section').fadeOut();
                                        $('#finish_section').removeClass('open');
                                        $('#pop_start .letsCheckit').attr("data-mainDocId", res.fk_doc_main_id);
                                        $('#pop_start .letsCheckit').attr("data-detailDocId", res.fk_doc_details_id);
                                        $('#pop_start').fadeIn('slow');
                                    }

                                    $('.pop_loding').hide();
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    $('.pop_loding').hide();
                                    $('#finish_section').fadeOut();
                                    $('#finish_section').removeClass('open');
                                    console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                                }
                            });
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $('.pop_loding').hide();
                        console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                    }
                });
                return false;
            } else
            {
                $('div.submit_area').append('<p class="spn_submited_doc_error" style="color:red;"> Document should not be blank </p>');
                setTimeout(function () {
                    $('.spn_submited_doc_error').fadeOut('slow');
                }, 2000);
            }
        });

        $('.letsCheckit').click(function () {

            location.reload();
            return false;

        });

        $('#look_perfect .save_doc .btnyes').click(function (e) {

            $('.docmsg').remove();
            $('.pop_loding').show();

            var desc = $('#txt_area_upload_doc').html().trim();
            desc = desc.replace(/<br>/g, "\n");
            desc = desc.replace(/<div>/g, "");
            desc = desc.replace(/<\/div>/g, "\n");
            //desc = desc.replace(/\s+/g, ' ');

            var data = new FormData();
            var fk_cust_id = $("#hdnCustomerId").val();
            var fk_main_doc_id = $("#hdnMainDocId").val();
            var fk_sub_doc_id = $("#hdnSubDocId").val();
            var docid = $("#hdndocidpartsid").val();
            var changed_wrod_ary = get_diff_str_array(doc_detail, desc);

            $.ajax({
                type: 'POST',
                url: '<?php echo admin_url('admin-ajax.php'); ?>',
                method: "post",
                data: {
                    action: 'updateDocStatus',
                    doc_id: docid,
                    changed_wrod_ary: changed_wrod_ary,
                    word_desc: desc,
                    fk_cust_id: fk_cust_id,
                    fk_main_doc_id: fk_main_doc_id,
                    fk_sub_doc_id: fk_sub_doc_id
                },
                success: function (data2) {
                    if (data2 != 0) {

                        $(".doc_msg").html('<span class="text-success docmsg">Document submitted successfully...</span>');
                        window.setTimeout(function () {
                            $('#look_perfect').fadeOut();
                            $('#look_perfect').removeClass('open');
                            $('#pop_start_Confirm').fadeIn('slow');
                        }, 1000);
                    } else {
                        $(".doc_msg").html('<span class="text-danger docmsg">Please try again.</span>');
                        window.setTimeout(function () {
                            $('#look_perfect').fadeOut();
                            $('#look_perfect').removeClass('open');
                            $('.double_msg').fadeOut('slow');
                        }, 1000);
                    }
                    $('.pop_loding').hide();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('.pop_loding').hide();
                    $('#look_perfect').fadeOut('slow');
                    $('#look_perfect').removeClass('open');
                    console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                }
            });
        });

        $("#btnLooksPerfect").click(function () {

            $('.double_msg').remove();
            $('#look_perfect').fadeIn();
            $('#look_perfect').addClass('open');
            return false;
        });

        $("#btnFinishedMySection").click(function ()
        {
            var desc = $("#txt_area_upload_doc").html().trim();
            desc = desc.replace(/<br>/g, "\n");
            desc = desc.replace(/<div>/g, "");
            desc = desc.replace(/<\/div>/g, "\n");
            //desc = desc.replace(/\s+/g, ' ');
            if (desc != "") {
                $('#finish_section').fadeIn();
                $('#finish_section').addClass('open');
            } else
            {
                $('div.submit_area').append('<p class="spn_submited_doc_error" style="color:red;"> Document should not be blank </p>');
                setTimeout(function () {
                    $('.spn_submited_doc_error').fadeOut('slow');
                }, 2000);
            }
            return  false;
        });
    });

    $(document).on("click", ".proof_dashboard_accor .wpb_wrapper .vc_toggle_simple", function () {
        var id = $(this).attr("id");
        $(".proof_dashboard_accor .wpb_wrapper .vc_toggle_simple:not(#" + id + ")").removeClass('vc_toggle_active');
        $(".proof_dashboard_accor .wpb_wrapper .vc_toggle_simple:not(#" + id + ")").find(".vc_toggle_content").hide();
    });

</script>
