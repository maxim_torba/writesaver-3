<?php
/*
  Template Name: Register */
if (is_user_logged_in()):
    echo '<script>window.location.href="' . get_site_url() . '"</script>';
    exit;
endif;
get_header();
?>

<style>
    .form-error
    {
        color: red;
        float: left;
        display:none;
        width: 100%;
        margin-top: 5px;
    }
    span.reg_msg
    {
        float: left;

    }
    .remember {
        text-align: left;
        float: left;
        width: 100%;
    }
</style>
<div class="cust_regi">
    <section class="login" id="register_section">
        <div class="breadcum">
            <div class="container">
                <div class="page_title">
                    <h1>Create your Account</h1>
                </div>
                <div class="row">
                    <div class="col-sm-offset-2 col-sm-8">
                        <div class="login_form register">
                            <form name="signup" id="cust_signup" method="post" action="">
                                <input type="hidden" name="role" class="role" value="customer">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input type="text" placeholder="First Name*" class="only_alpha contact_block" maxlength="50" name="fname" required="">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" placeholder="Last Name*" class="only_alpha contact_block" maxlength="50" name="lname" required="">
                                    </div>
                                </div>
                                <input type="text" placeholder="Email*" class="contact_block" name="email" maxlength="50">
                                <input type="password" maxlength="20" placeholder="Password*" class="contact_block" name="pw" id="cupw" required="">
                                <input type="password" maxlength="20" placeholder="Confirm Password*" class="contact_block" name="cpw"  required="">
                                <div class="remember">
                                    <input id="cust_remember" type="checkbox" name="remember" /><label for="cust_remember">I agree with the <span><a href="<?php echo get_page_link(35); ?>">terms</a></span> and <span><a href="<?php echo get_page_link(33); ?>">privacy policy</a></span></label>
                                    <span class="form-error text-danger " id="spn_ChkAgree" >Please accept the terms of service before continuing</span>
                                </div>
                                <div class="sub_btn">
                                    <input type="submit" name="signup" id="signupbtn" class="btn_sky" value="Register">
                                </div>
                                <div class="msg" id="signup_msg" ></div>
                            </form>
                            <div class="other_login">
                                <span>or</span>
                            </div>
                            <div class="social_login row">
                                <?php echo do_shortcode('[TheChamp-Login]'); ?>
                            </div>
                            <p>
                                Already have an account?  <a href="javascript:void(0);" class="open_login" onclick="open_loginpop('login');">Login</a>
                                <a class="login_url" href="<?php echo get_page_link(545); ?>"  style="display:none;">Login</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div id="thankyou_section" style="display: none;">
        <section class="login">
            <div class="breadcum">
                <div class="container">
                    <div class="page_title">
                        <h1>Thank you</h1>
                    </div>
                    <div class="registration_thank_you">
                        <div class="row">
                            <div class="col-md-offset-3 col-sm-6">
                                <div class="thank-you">
                                    <div class="thank_msg">
                                        <div class="thank_img">
                                            <img src="<?php echo get_template_directory_uri() ?>/images/check_thanku.png" alt="images">
                                        </div>
                                        <h2>Registration successful! Confirm Your account now to access Writesaver.</h2>
                                        <p>We sent a confirmation email to <span class="user_mail"></span>. Check your inbox and press the confirmation button to access your account.</p>
                                        <div class="thank_confirm_email">
                                            <p>Didn't get your Confirmation Email?</p>
                                            <p>Check your spam folder.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<script>

    jQuery(document).ready(function () {
        $("#cust_signup").validate({
            errorElement: 'span', //default input error message container
            errorClass: 'text-danger reg_msg', // default input error message class  
            rules: {
                fname: "required",
                lname: "required",
                email: {
                    required: true,
                    email: true
                },
                pw: {
                    required: true,
                    minlength: 6,
                    pwcheck: true,
                },
                cpw: {
                    required: true,
                    equalTo: "#cupw",
                    pwcheck: true,
                    minlength: 6,
                }
            },
            messages: {
                fname: {
                    required: "First name is required."
                },
                lname: {
                    required: "Last name is required."
                },
                email: {
                    required: "Email is required.",
                    email: "Please enter a valid email address.",
                },
                pw: {
                    required: "Password is required.",
                    minlength: jQuery.validator.format("minimum {0} characters are necessary"),
                    pwcheck: "Password must contain an uppercase and lowercase letter, a number and a special character"
                },
                cpw: {
                    required: "Confirm password is required.",
                    equalTo: "Passwords do not match"
                }
            }, submitHandler: function (form) {

                if ($("#cust_remember").is(':checked') == false)
                {
                    $("#cust_signup #spn_ChkAgree").show();
                    return false;
                } else
                {
                    $("#cust_signup #spn_ChkAgree").hide();
                }
                $('#loding').show();
                var alldata = $('#cust_signup').serialize();
                $.ajax({
                    url: '<?php echo admin_url('admin-ajax.php'); ?>',
                    type: "POST",
                    data: alldata + '&action=user_signup',
                    dataType: "html",
                    success: function (data) {
                        if (data == 1) {
                            $('.cust_regi span.user_mail').text($('.cust_regi input[name=email]').val());
                            $('.cust_regi #signup_msg').html('<span  class="text-success reg_msg" >A confirmation email has been send to your email account. Please click the link in the email to verify your account.</span>');
                            $('.cust_regi .reg_msg').fadeOut('slow');
                            $('.cust_regi #register_section').hide();
                            $('.cust_regi #thankyou_section').show();
                        } else if (data == 2) {
                            $('.cust_regi #signup_msg').html('<span  class="text-danger reg_msg" >There is already an account associated with this email address. Please login to continue.</span>');
                            setTimeout(function () {
                                $('.cust_regi .reg_msg').fadeOut('slow');
                            }, 5000);
                        } else {
                            $('.cust_regi #signup_msg').html('<span  class="text-danger reg_msg" >An error occur while registering. Please try again.</span>');

                            setTimeout(function () {
                                $('.cust_regi .reg_msg').fadeOut('slow');
                            }, 5000);
                        }
                        $('#loding').hide();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {

                        $('#loding').hide();
                        console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                    }
                });
                return false;
            }
        });
        $.validator.addMethod("pwcheck", function (value) {
            return /^[A-Za-z0-9\d!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]*$/.test(value)
                    && /[A-Z]/.test(value)
                    && /[a-z]/.test(value)
                    && /\d/.test(value)
                    && /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/.test(value)

        });
    });
</script>
<?php get_footer(); ?>
