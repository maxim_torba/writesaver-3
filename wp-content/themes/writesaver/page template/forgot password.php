<?php
/*
  Template Name: Forgot Password
 */
get_header();
if (is_user_logged_in()) {
    echo '<script>window.location.href="' . site_url() . '"</script>';
    exit;
}
?>
<style>
    .form-error
    {
        color: red;
        float: left;
        display:none;
        width: 100%;
        margin-top: 5px;
    }
    #forget_pw span
    {
        float: left;       
        font-weight: normal;
    }
    .remember {
        text-align: left;
        float: left;
        width: 100%;
    }
</style>  
<section class="login">
    <div class="breadcum">
        <div class="container">
            <div class="page_title">
                <h1>Forgot Password</h1>
            </div>
            <div class="row">
                <div class="col-sm-offset-3 col-sm-6">
                    <div class="login_form">
                        <form name="" id="forget_pw" method="post" action="">
                            <input type="email" placeholder="Email*" class="contact_block" name="email" id="email" required="">                                   
                            <div class="msg" id="forgot_msg" ></div>
                            <div class="sub_btn">   
                                <input type="submit" class="btn_sky" value="Submit" id="forgate_sub" onclick="submit_forget_pw()">   
                            </div>  
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
<script type="text/javascript">
    jQuery(document).ready(function () {


        $("#forget_pw").validate({
            errorElement: 'span', //default input error message container
            errorClass: 'text-danger login_msg', // default input error message class  
            rules: {
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                email: {
                    required: "Email is required.",
                    email: "Please enter a valid email."
                },
            },
            submitHandler: function (form) {
                $('#loding').show();
                $('.login_msg').remove();
                var alldata = $('#forget_pw').serialize();
                $.ajax({
                    url: '<?php echo admin_url('admin-ajax.php'); ?>',
                    type: "POST",
                    data: alldata + '&action=forgot_pass',
                    //dataType: "html",
                    success: function (data) {

                        if (data == 1) {
                            $('#forgot_msg').html('<span  class="text-success login_msg" >A password reset email has been sent to you.<span>');
                            $('#loding').hide();
                            setTimeout(function () {
                                $('.login_msg').fadeOut('slow');
                            }, 5000);

                            //window.location.href = "<?php echo site_url(); ?>";
                        } else {
                            $('#forgot_msg').html('<span  class="text-danger login_msg" >Email id does not exists.</span>');
                            $('#loding').hide();
                            setTimeout(function () {
                                $('.login_msg').fadeOut('slow');
                            }, 5000);
                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $('#loding').hide();
                        console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                    }
                });
                return false;
            }
        });



    });
</script>
