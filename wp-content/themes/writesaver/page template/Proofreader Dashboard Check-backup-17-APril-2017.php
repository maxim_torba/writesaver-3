<?php
/*
 * Template Name: 17-APril-2017
 */

get_header();

global $wpdb;
$datetime = date('Y-m-d H:i:s');
$user_id = get_current_user_id();

$result = $wpdb->get_results("SELECT * FROM wp_assigned_document_details WHERE fk_proofreader_id= $user_id AND status='In Process' ORDER BY pk_assigned_id DESC LIMIT 1 ");
$assinged_proofreaderid = $result[0]->fk_proofreader_id;
$assinged_doc_proofreader_name = $wpdb->get_results("SELECT display_name from wp_users where ID= $assinged_proofreaderid ");

//$resultothersdocs = $wpdb->get_results("SELECT * FROM tbl_proofreaded_doc_details where Fk_DoubleProofReader_Id= $user_id and DoubleCheckStatus=0");

if (count($result) > 0) {

    $doc_details_id = $result[0]->fk_doc_details_id;
    $doc_main_id = $result[0]->fk_doc_main_id;
    $result_doc = $wpdb->get_results("SELECT * FROM wp_customer_document_details WHERE pk_doc_details_id= $doc_details_id ");

    $result_maindoc_name = $wpdb->get_var("SELECT document_title FROM wp_customer_document_main WHERE pk_document_id= $doc_main_id ");


//    echo 'IF=';
//    echo '<pre>';
//    print_r($result_doc);

    $status = $result_doc[0]->status;

    $cust_id = $result_doc[0]->fk_cust_id;
    $proof_id = $user_id;
    $desc = "We've started editing your document!";
    if ($status == "pending") {

        echo send_cust_notification($cust_id, $proof_id, $desc);
    }
} else {

    $result_doc = $wpdb->get_results(" SELECT * FROM wp_customer_document_details where status='pending' and is_active=1 LIMIT 1 ");
    $status = $result_doc[0]->status;

    $doc_main_id = $result_doc[0]->fk_doc_main_id;
    $result_maindoc_name = $wpdb->get_var("SELECT document_title FROM wp_customer_document_main WHERE pk_document_id= $doc_main_id ");


    $cust_id = $result_doc[0]->fk_cust_id;
    $proof_id = $user_id;

    $desc = "We've started editing your document!";
//    if ($status == "pending") {
//        
//        echo send_cust_notification($cust_id, $proof_id, $desc);
//    }
//
//    $cust_info = get_userdata($cust_id);
//    $proof_info = get_userdata($proof_id);
//
//    $to = $cust_info->user_email;
//    $headers = "MIME-Version: 1.0" . "\r\n";
//    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
//    $headers .= 'From: Writesaver' . "\r\n";
//    wp_mail('ankita.vekariya@aliansoftware.com', $desc, $desc, $headers);
//
//    global $wpdb;
//    $wpdb->insert(
//            'tbl_customer_notifications', array(
//        'notification_date' => date('Y-m-d H:i:s'),
//        'fk_customer_id' => $cust_id,
//        'fk_proofreader_id' => $proof_id,
//        'description' => $desc
//            )
//    );

    $wpdb->update(
            'wp_customer_document_details', array(
        'status' => 'In Process',
        'modified_date' => $datetime
            ), array(
        'pk_doc_details_id' => $result_doc[0]->pk_doc_details_id)
    );
    if (count($result_doc) > 0) {
        $wpdb->insert(
                'wp_assigned_document_details', array(
            'fk_doc_details_id' => $result_doc[0]->pk_doc_details_id,
            'fk_doc_main_id' => $result_doc[0]->fk_doc_main_id,
            'fk_cust_id' => $result_doc[0]->fk_cust_id,
            'fk_proofreader_id' => $user_id,
            'assign_date' => $datetime,
            'status' => 'In Process',
            'created_date' => $datetime
                )
        );
    }
}

$usersDtl = array(226, "proofreader");
array_push($usersDtl, 225, "customer");
?>
<section>
    <div class="breadcum">
        <div class="container">
            <div class="page_title">
                <h1>Dashboard</h1>
            </div>
        </div>
    </div>
</section>
<section class="proof privacy">
    <div class="container">
        <div class="doc_name">
            <h2><?php echo $result_maindoc_name; ?></h2>
        </div>

        <div class="main_editor">
            <form id="frmSubmitedDoc" name="frmSubmitedDoc">
                <div class="editor_top">
                    <div class="editor_inner_top">                            
                        <div class="used_word">
                            <span>Word Count:</span>&nbsp;<span class="count"><?php echo str_word_count($result_doc[0]->document_desc); ?></span>
                        </div>
                    </div>
                    <input type="hidden" id="hdnProofreaderName" name="hdnProofreaderName" value="<?php echo $assinged_doc_proofreader_name[0]->display_name; ?>">
                    <div class="hidden_scroll check">
                        <div class="changeable check " contenteditable="true" id="txt_area_upload_doc" data-id="<?php echo $result_doc[0]->pk_doc_details_id; ?>" style="width: 100%; height:500px; display: inline-block;white-space: pre-line;" >
                            <?php echo trim($result_doc[0]->document_desc); ?>
                        </div>


                        <div class="changeable check" id="doublcheck_section" contenteditable="false" data-id="">  

                        </div>
                        <div id="original" style="white-space: pre-line;position: relative;display:none;height:500px; width: 100%;white-space: pre-line; display:none;">
                            <?php echo trim($result_doc[0]->document_desc); ?>
                        </div>
                        <div id="temdoc" style="white-space: pre-line;position: relative;display:none;"><?php echo trim($result_doc[0]->document_desc); ?>
                        </div>
                        <div id="deletedwords" style="display: none;"></div>
                        <div id="deletedwordsbackspace" style="display: none;"></div>
                        <input type="hidden" id="hdndocidpartsid" name="hdndocidpartsid" value="<?php echo $result_doc[0]->pk_doc_details_id; ?>" />
                        <input type="hidden" id="fk_proofreader_id" name="fk_proofreader_id" value="<?php echo $assinged_proofreaderid ?>" />
                    </div>
                </div>
                <div class="submit_area">
                    <div class="btn_blue">
                        <a href="javascript:void(0);" class="btn_sky" id="btnFinishedMySection">Finished my section</a>
                        <a href="javascript:void(0);" class="btn_sky" id="btnLooksPerfect" style="display: none;">Looks Perfect!</a>
                        <input type="hidden" id="hdnMainDocId" name="hdnMainDocId" value="<?php echo $result_doc[0]->fk_doc_main_id; ?>">
                        <input type="hidden" id="hdnSubDocId" name="hdnSubDocId" value="<?php echo $result_doc[0]->pk_doc_details_id; ?>">
                        <input type="hidden" id="hdnCustomerId" name="hdnCustomerId" value="<?php echo $result_doc[0]->fk_cust_id; ?>">
                    </div>
                </div>
            </form>
        </div>
        <div class="row service">
            <div class="col-sm-4">
                <div class="total_ammount">
                    <div class="left">
                        <h4>83<span>Docs.</span></h4>
                        <div class="divider"></div>
                        <p>Total <br>Docs. worked on</p>
                    </div>
                    <div class="right"></div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="total_ammount paid">
                    <div class="left">
                        <h4>20000<span>Words</span></h4>
                        <div class="divider"></div>
                        <p>Total<br> words edited</p>
                    </div>
                    <div class="right"></div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="total_ammount remaining">
                    <div class="left">
                        <h4>$500<span>Money</span></h4>
                        <div class="divider"></div>
                        <p>Total<br> earnings</p>
                    </div>
                    <div class="right"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="pop_start" class="pop_overlay open start_tests" style="display: none;">
    <div class="pop_main">
        <div class="pop_head">
            <a href="javascript:void(0);"><i class="fa fa-remove" aria-hidden="true"></i></a>
        </div>
        <div class="pop_body">
            <div class="page_title">
                <h2>Double check</h2>
            </div>
            <div class="pop_content">
                <div class="first_test">
                    <p>Other part has already been proofread, but please double check for mistakes!</p>
                    <a href="#" class="btn_sky pop_btn letsCheckit">Let’s Check it!</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="pop_start_Confirm" class="pop_overlay open start_tests ready" style="display: none;">
    <div class="pop_main">
        <div class="pop_head">
            <a href="javascript:void(0);"><i class="fa fa-remove" aria-hidden="true"></i></a>
        </div>
        <div class="pop_body">
            <div class="page_title">
                <h2>Completely proofread</h2>
            </div>
            <div class="pop_content">
                <div class="first_test">
                    <p>This document has completely proofread.</p>
                    <a href="<?php echo get_page_link(768); ?>" class="btn_sky orange pop_btn btnReturnToProfile">Return to profile</a>
                    <a href="<?php echo get_page_link(810); ?>" class="btn_sky pop_btn btnStartNextSubmission">Start on next submission!</a>                            
                </div>
            </div>
        </div>
    </div>
</div>
<?php
if (have_posts()) :
    while (have_posts()) : the_post();
        the_content();
    endwhile;
endif;
?>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js" type="text/javascript"></script>-->
<!--<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>-->   
<script src="<?php echo get_template_directory_uri() ?>/js/node_modules/socket.io-client/socket.io.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/nodeClient.js"></script>
<?php get_footer(); ?>
<script>


    var lstedited = [];
    var lstoriginal = [];
    var listofwordschanged = [];
    var deletebackspacewords = [];
    var getdeletedWordsfinal = [];
    function getlines()
    {

        var enteredoriginalText = $("#original").html().trim();
        var numberOfLineBreaksoriginal = (enteredoriginalText.match(/\n/g) || []).length;
        var orginal_numbers_line = numberOfLineBreaksoriginal;
        var lines = $("#original").html().trim().split("\n");

        var row = 0;
        $.each(lines, function (n, elem) {
            if (elem != "" && elem != null)
            {
                var colms = 0;
                if (elem.indexOf(' ') > -1)
                {
                    var strbreakwords = elem.split(' ');
                    for (i = 0; i < strbreakwords.length; i++)
                    {
                        colms = i;
                        lstoriginal.push([row, elem.toString().trim(), colms, strbreakwords[i]]);
                    }
                } else
                {
                    //only one word
                    lstoriginal.push([row, elem.toString().trim(), colms, elem.toString()]);
                }
                row++;
            }
        });

    }




    String.prototype.highLightAt = function (index) {
        return  this.substr(index, 1);
    }

    $(window).load(function () {
        getlines();
    });





    function GetOffset(offset, length, currenttext)
    {
        var strcontents = CleardivForNewline($("#temdoc").html().trim());

        if (strcontents.substr(offset, length).indexOf(" ") != -1) {
            var contents = strcontents.substr(offset, length);
            var lastchar = contents.substr(contents.length - 1);
            var firstchar = contents.substr(0, 1);
            if (firstchar == " ") {
                if (length != 1) {
                    offset = parseInt(offset) + 1;
                    newstring = strcontents.substr(offset, length);
                } else {
                    offset = offset;
                    newstring = strcontents.substr(offset, length);
                }
            }
            else if (lastchar == " ") {
                if (length != 1) {
                    offset = parseInt(offset) - 1;
                    newstring = strcontents.substr(offset, length);
                } else {
                    offset = offset;
                    newstring = strcontents.substr(offset, length);
                }
            } else {
                var newstring = strcontents.substr(offset, length);
                while (currenttext.trim() != newstring.trim()) {
                    offset = parseInt(offset) - 1;
                    newstring = strcontents.substr(offset, length);
                }
            }
        } else {
            var newstring = strcontents.substr(offset, length);

            while (currenttext.trim() != newstring.trim()) {
                offset = parseInt(offset) + 1;
                newstring = strcontents.substr(offset, length);
            }

        }
        $("#temdoc").html($('#txt_area_upload_doc').html());
        return  offset + "_" + newstring;
    }
    function CleardivForNewline(originalhtml) {

        var str = originalhtml;

        var stralllines = str.split("\n")
        var finalvalues = "";
        for (var i = 0; i < stralllines.length; i++) {
            if (finalvalues == "") {
                finalvalues = stralllines[i];
            } else {
                if (stralllines[i] == "" || stralllines[i] == null) {
                    finalvalues = finalvalues + "\n " + stralllines[i];
                } else {
                    finalvalues = finalvalues + "\n " + stralllines[i];
                }
            }
        }
        return finalvalues;
    }
    $(document).ready(function () {
        var temparrybackspacechnaged = [];
        $('#txt_area_upload_doc').keydown(function (e) {
            var updatednew = "";
            var userSelection;
            if (window.getSelection) {
                userSelection = window.getSelection();
            }
            var start = userSelection.anchorOffset;
            var end = userSelection.focusOffset;
            var position = [start, end];
            var val = $(this).text();

            //new logic for old words get

            var oldstringnewlogic;
            if (e.keyCode == 8) {


                var startpostions = start;
                var endpostions = end;

                var temptext = $("#original").text().trim();

                while (temptext != " ")
                {
                    startpostions = parseInt(startpostions) - 1;
                    temptext = val.substring(startpostions, start);
                    temptext = temptext.substr(0, 1);
                }
                temptext = $("#original").text().trim();

                while (temptext != " ")
                {
                    endpostions = parseInt(endpostions) + 1;
                    temptext = val.substring(end, endpostions);
                    temptext = temptext.substr(temptext.length - 1);
                }

                //alert(val.substring(startpostions, endpostions));
                oldstringnewlogic = val.substring(startpostions, endpostions);
                if (start >= startpostions && end <= endpostions)
                {
                    if (temparrybackspacechnaged.length == 0)
                    {
                        temparrybackspacechnaged.push([startpostions, endpostions, oldstringnewlogic]);
                    }
                    else
                    {
                        for (var kc = 0; kc <= temparrybackspacechnaged.length - 1; kc++)
                        {
                            if (startpostions > temparrybackspacechnaged[kc][0] && endpostions < temparrybackspacechnaged[kc][1])
                            {
                                temparrybackspacechnaged.push([startpostions, endpostions, oldstringnewlogic]);
                                break;
                            }
                        }
                    }
                }


            }
            //end for new logic
            var length = parseInt(end) - parseInt(start);
            if (length == 0)
            {
                length = 1;

            }
            //var getoffsetandstring = GetOffset(start, length, val.substring(position[0], position[1])); //commented

            var offset = start; //chnaged
            var oldstring = ""; //changed

            var deleted = '';
            if (e.keyCode == 8) {

                if (position[0] == position[1]) {
                    if (position[0] == 0)
                    {
                        deleted = '';
                        offset = offset;
                        length = length;
                        oldstring = deleted;
                    }
                    else
                    {
                        deleted = val.substr(parseInt(position[0]) - 1, 1);
                        offset = parseInt(position[0]) - 1;

                        oldstring = deleted;
                    }
                }
                else {
                    deleted = val.substring(position[0], position[1]);
                    offset = position[0];
                    oldstring = deleted;
                }

                updatednew = deleted;
                console.log("backspace call" + offset + " " + length + " " + oldstring + " " + updatednew + " " + "Delete BackSpace");
                // oldstring = oldstringnewlogic;

                for (var i = 0; i < listofwordschanged.length - 1; i++)
                {
                    if (listofwordschanged[i] >= offset && offset <= listofwordschanged[i])
                    {
                        listofwordschanged[i] = parseInt(listofwordschanged[i]) - 1;
                        //    oldstring = val.substr(offset, length);
                    }
                }

                listofwordschanged.push([offset, length, oldstring, updatednew, "Delete BackSpace"]);
                //comparedocs();

            }
            else if (e.keyCode == 46) {

                var val = $(this).text();
                if (position[0] == position[1]) {
                    if (position[0] === val.length)
                        deleted = '';
                    else
                        deleted = val.substr(position[0], 1);
                }
                else {
                    deleted = val.substring(position[0], position[1]);
                }
                //console.log("deleted words call");
                updatednew = deleted;
                oldstring = deleted;
                offset = position[0];
                // alert(deleted);

                for (var i = 0; i < listofwordschanged.length - 1; i++)
                {
                    if (listofwordschanged[i] >= offset && offset <= listofwordschanged[i])
                    {
                        listofwordschanged[i] = parseInt(listofwordschanged[i]) - 1;
                        //  oldstring = val.substr(offset, length);
                    }
                }
                listofwordschanged.push([offset, length, oldstring, updatednew, "Delete"]);
                //comparedocs();

            } else if (e.which !== 0) {

                var c = String.fromCharCode(e.which);
                var isWordCharacter = c.match(/\w/);
                if ((isWordCharacter)) {
                    var val = $(this).text();

                    //  console.log("String.fromCharCode(e.which).toLowerCase() " + String.fromCharCode(e.which).toLowerCase());
                    updatednew = String.fromCharCode(e.which).toLowerCase();
                    oldstring = oldstring;
                    length = 1;
                    offset = position[0];


                    var tmpstartpostions = parseInt(position[0]) - 1;
                    var tmpstartstring = val.highLightAt(tmpstartpostions);

                    var removestart = 0;
                    var preveiousstrings = "";
                    while (tmpstartstring.trim().length > 0)
                    {
                        if (tmpstartstring.trim().length != 0)
                        {
                            tmpstartpostions = parseInt(tmpstartpostions) - 1;
                            preveiousstrings += tmpstartstring;
                        }
                        removestart++;
                        tmpstartstring = val.highLightAt(tmpstartpostions);


                    }


                    oldstring = "";
                    updatednew = "";
                    var tmpendpostions = parseInt(position[0]) - removestart;
                    offset = tmpendpostions - 1;
                    var tmpendstring = val.highLightAt(tmpendpostions);

                    preveiousstrings += tmpendstring;
                    while (tmpendstring.trim().length > 0)
                    {
                        tmpendstring = val.highLightAt(tmpendpostions);
                        // alert("end" + (tmpendstring));

                        tmpendpostions = parseInt(tmpendpostions) + 1;
                        if (tmpendpostions != position[0])
                        {
                            oldstring += tmpendstring;
                            updatednew += tmpendstring;
                        } else {
                            oldstring += tmpendstring;
                            updatednew += tmpendstring + String.fromCharCode(e.which).toLowerCase();
                        }


                    }
                    var endoffset = parseInt(tmpendpostions);
                    // alert(oldstring.length);
                    length = updatednew.length - 1;
                    //added 30-march-2017
                    if (length < 0)
                    {
                        length = 1;
                        oldstring = "";
                        updatednew = $('#txt_area_upload_doc').text().substr(offset, length);
                    }
                    //added still here 30-march-2017

                    for (var kc = 0; kc <= temparrybackspacechnaged.length - 1; kc++)
                    {
                        if (start >= temparrybackspacechnaged[kc][0] && end <= temparrybackspacechnaged[kc][1])
                        {
                            oldstringnewlogic = temparrybackspacechnaged[kc][2];
                            break;
                        } else if (start == end)
                        {
                            oldstringnewlogic = temparrybackspacechnaged[kc][2];
                            break;
                        }
                    }
                    temparrybackspacechnaged.push([offset, parseInt(offset) + parseInt(length), oldstringnewlogic]);
                    for (var i = 0; i < listofwordschanged.length - 1; i++)
                    {
                        if (listofwordschanged[i] >= offset && offset <= listofwordschanged[i])
                        {
                            listofwordschanged[i] = parseInt(listofwordschanged[i]) + 1;
                            //   oldstring = val.substr(offset, length);
                        }
                    }
                    console.log(start + " end " + end + " changed:" + " " + offset + " " + length + " oldstring " + oldstring + " " + updatednew + " " + "Changed or Added");
                    listofwordschanged.push([offset, length, oldstring, updatednew, "Changed or Added"]);

                } else {
                    //up and down and other key pressed so don't need to track
                    //alert("notword");
                }
            }
            $("#deletedwords").text(deleted);


        });


    });

    $('.pop_head a').click(function () {
        $('#pop_start').fadeOut('slow');
    });
    $('.letsCheckit').click(function () {
        $('#pop_start').fadeOut('slow');
    });

    $("#btnLooksPerfect").click(function () {

        $('#pop_start_Confirm').fadeOut('slow');
        $('#pop_start_Confirm').css("display", "none");

        var inProcessDocDtlId = $("#txt_area_upload_doc").attr("data-id");
        var doubleCheckDocDtlId = $("#txt_area_upload_doc").attr("data-id");

        $.ajax({
            type: 'POST',
            url: '<?php echo admin_url('admin-ajax.php'); ?>',
            method: "post",
            data: {
                action: 'updateDocStatus',
                inProcessDocId: $("#txt_area_upload_doc").attr("data-id"),
                doubleCheckDocId: $("#doublcheck_section").attr("data-id")
            },
            type: 'POST',
                    success: function (data2) {
                        $('#pop_start_Confirm').fadeIn('slow');
                    },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#pop_start_Confirm').fadeOut('slow');
                $('#pop_start_Confirm').css("display", "none");
                alert(jqXHR + " :: " + textStatus + " :: " + errorThrown);
            }
        });
    });

    $("#btnFinishedMySection").click(function ()
    {
        var desc = $("#txt_area_upload_doc").text();
        var data = new FormData();

        var fk_cust_id = $("#hdnCustomerId").val();
        var fk_main_doc_id = $("#hdnMainDocId").val();
        var fk_sub_doc_id = $("#hdnSubDocId").val();

        var fk_proofreader_id = $("#fk_proofreader_id").val();
        var docid = $("#hdndocidpartsid").val();

        var listofwordschangedFinal = [];
        var listofwordschangedFinalMain = [];
        var temparray = [];
        var updatedWordsAry = [];

        listofwordschanged.sort(function (a, b) {
            var aVal = parseInt(a[0]) + parseInt(a[1]),
                    bVal = parseInt(b[0]) + parseInt(b[1]);
            return bVal - aVal;
        });
        for (var k = 0; k <= listofwordschanged.length - 1; k++)
        {

            var sourceId = listofwordschanged[k][0];
            var sourceId1 = listofwordschanged[k][1];
            var sourceId2 = listofwordschanged[k][2];
            var wrd = listofwordschanged[k][2];
            var item = temparray.filter(function (collect) {
                return collect[0] == sourceId && collect[3].toString().trim() == wrd.toString().trim(); //&& collect[1] == sourceId1 && collect[2] == sourceId2 // check offset added or not 
            });
            if (item.length == 0)
            {
                if (listofwordschanged[k][4].toString().trim() != "Delete BackSpace" && listofwordschanged[k][4].toString().trim() != "Delete")
                {

                    var newUser = [];
                    newUser.push(listofwordschanged[k][0], listofwordschanged[k][1], listofwordschanged[k][2], listofwordschanged[k][3], listofwordschanged[k][4]);
                    // temparray.push(newUser);
                    updatedWordsAry.push([listofwordschanged[k][0], listofwordschanged[k][1], listofwordschanged[k][2], listofwordschanged[k][3], listofwordschanged[k][4]]);
                    //console.log("<br/>\n updated :-  " + k + 1 + " " + listofwordschanged[k][0], listofwordschanged[k][1], listofwordschanged[k][2], listofwordschanged[k][3], listofwordschanged[k][4]);

                }
                else if (listofwordschanged[k][4].toString().trim() == "Delete")
                {
                    var newUser = [];
                    newUser.push(listofwordschanged[k][0], listofwordschanged[k][1], listofwordschanged[k][2], listofwordschanged[k][3], listofwordschanged[k][4]);
                    temparray.push(newUser);
                    // console.log("<br/>\n Final delete:-  " + k + 1 + " " + listofwordschanged[k][0], listofwordschanged[k][1], listofwordschanged[k][2], listofwordschanged[k][3], listofwordschanged[k][4]);
                }
            }
        }

        var finalDeleteBackSpaceword = [];
        var item = listofwordschanged.filter(function (collect) {
            return collect[4] == "Delete BackSpace";
        });
        if (item.length > 0)
        {
            item.sort(function (a, b) {
                var aVal = parseInt(a[0]) + parseInt(a[1]),
                        bVal = parseInt(b[0]) + parseInt(b[1]);
                return aVal - bVal;
            });
            var temp = 0;
            var str = "";
            var offsetstart = 0;
            var length = 0;
            for (var k = 0; k <= item.length - 1; k++)
            {
                var finalword = "";
                var newindex = parseInt(item[k][0]) + 1;
                var previousindex = parseInt(item[k][0]) - 1;
                var item1 = item.filter(function (collect) {
                    return collect[0] == newindex;
                });

                if (item1.length > 0)
                {
                    if (temp == 0)
                    {
                        offsetstart = item[k][0];
                        length = 1;
                    } else {
                        length = parseInt(length) + 1;
                    }
                    str += item[k][2];
                    temp = parseInt(temp) + 1;
                    // console.log("str" + str);
                } else
                {

                    var item2 = item.filter(function (collect) {
                        return collect[0] == previousindex;
                    });
                    if (item2.length == 0)
                    {
                        if (temp == 0)
                        {
                            offsetstart = item[k][0];
                            length = 1;
                        } else {
                            length = parseInt(length) + 1;
                        }

                        str = item[k][2];
                        finalword = str;
                        str = "";
                        temp = 0;

                    } else {
                        str += item[k][2];
                        length = parseInt(length) + 1;

                        finalword = str;
                        str = "";
                        temp = 0;
                    }
                    // console.log("str" + str);

                    //k=temp;

                }
                if (str == "")
                {
                    //  console.log("<br/>\n DelBackSpace  :-  " + parseInt(k) + 1 + " " + offsetstart, length, finalword, finalword, item[k][4]);


                    var newUser = [];
                    newUser.push(offsetstart, length, finalword, finalword, item[k][4]);
                    finalDeleteBackSpaceword.push(newUser);
                    offsetstart = 0;
                    length = 0;
                }

            }

        }

        var tmpupdatedWordsAry = [];
        tmpupdatedWordsAry = updatedWordsAry;
        var oldstringary = [];
        var newstringary = [];
        var finalupdatedword = [];
        if (tmpupdatedWordsAry.length > 0 && updatedWordsAry.length > 0)
        {

//         updatedWordsAry=   updatedWordsAry.sort(function (a, b) {
//                var aVal = parseInt(a[0]) + parseInt(a[1]),
//                        bVal = parseInt(b[0]) + parseInt(b[1]);
//                return bVal - aVal;
//            });
//descending order

//alert(updatedWordsAry);
//           tmpupdatedWordsAry= tmpupdatedWordsAry.sort(function (a, b) {
//                var aVal = parseInt(a[0]) + parseInt(a[1]),
//                        bVal = parseInt(b[0]) + parseInt(b[1]);
//                return aVal - bVal;
//            });
//acending order
            var y = tmpupdatedWordsAry.sort(function (a, b) {
                return parseInt(a[0]) + parseInt(a[1]) > parseInt(b[0]) + parseInt(b[1]) ? 1 : -1;
            });
            //get oldword only by start offset
            for (var k = 0; k <= y.length - 1; k++)
            {
                var sourceId1 = y[k][0];
                var item1 = oldstringary.filter(function (collect) {
                    return collect[0] == sourceId1;
                });
                if (item1.length == 0)
                {
                    var newUser = [];
                    newUser.push(y[k][0], y[k][1], y[k][2], y[k][3], y[k][4]);
                    oldstringary.push(newUser);
                }

            }
            var x = updatedWordsAry.sort(function (a, b) {
                return parseInt(a[0]) + parseInt(a[1]) > parseInt(b[0]) + parseInt(b[1]) ? -1 : 1;
            });
            //get new word only by start offset
            for (var k = 0; k <= x.length - 1; k++)
            {
                var sourceId1 = x[k][0];
                var item1 = newstringary.filter(function (collect) {
                    return collect[0] == sourceId1;
                });
                if (item1.length == 0)
                {
                    var newUser = [];
                    newUser.push(x[k][0], x[k][1], x[k][2], x[k][3], x[k][4]);
                    newstringary.push(newUser);
                }
            }

            for (var k = 0; k <= newstringary.length - 1; k++)
            {
                var sourceId1 = newstringary[k][0];
                var item1 = finalupdatedword.filter(function (collect) {
                    return collect[0] == sourceId1;
                });
                if (item1.length == 0)
                {
                    var sourceId = newstringary[k][0];
                    var item = oldstringary.filter(function (collect) {
                        return collect[0] == sourceId; //check offset added or not
                    });
                    var oldstring = newstringary[k][2];
                    if (item.length > 0)
                    {
                        oldstring = item[0][2];

                    }
                    var id = parseInt(k) + 1;
                    var newUser = [];
                    newUser.push(newstringary[k][0], newstringary[k][1], oldstring, newstringary[k][3], newstringary[k][4]);
                    finalupdatedword.push(newUser);

                    //  console.log("<br/>\n updated :-  " + id + " " + updatedWordsAry[k][0], updatedWordsAry[k][1], oldstring, updatedWordsAry[k][3], updatedWordsAry[k][4]);
                }
            }



        }
//temparray  is an array for get delete words
        if (temparray.length > 0)
        {
            for (var k = 0; k <= temparray.length - 1; k++)
            {
                var newUser = [];
                newUser.push(temparray[k][0], temparray[k][1], temparray[k][2], temparray[k][3], temparray[k][4]);
                listofwordschangedFinalMain.push(newUser);
            }
        }

        // finalDeleteBackSpaceword  is an array for get back space deleted words
        if (finalDeleteBackSpaceword.length > 0)
        {
            for (var k = 0; k <= finalDeleteBackSpaceword.length - 1; k++)
            {
                // var wrd = getBackSpaceDeleteWords(finalDeleteBackSpaceword[k][2], finalDeleteBackSpaceword[k][0], finalDeleteBackSpaceword[k][1]);
                //  console.log("back wrd :  " + wrd + " back :-  " + finalDeleteBackSpaceword[k][0], finalDeleteBackSpaceword[k][1], finalDeleteBackSpaceword[k][2], finalDeleteBackSpaceword[k][3], finalDeleteBackSpaceword[k][4]);
                var newUser = [];
                newUser.push(finalDeleteBackSpaceword[k][0], finalDeleteBackSpaceword[k][1], finalDeleteBackSpaceword[k][2], finalDeleteBackSpaceword[k][3], finalDeleteBackSpaceword[k][4]);
                listofwordschangedFinalMain.push(newUser);
            }
        }

        // finalupdatedword  is an array for get updated words
        if (finalupdatedword.length > 0)
        {
            for (var k = 0; k <= finalupdatedword.length - 1; k++)
            {
                var offset = finalupdatedword[k][0] + finalupdatedword[k][1];
                var word = finalupdatedword[k][3];
                var itemDeleteBackSpaceword = finalDeleteBackSpaceword.filter(function (collect) {
                    return ((parseInt(collect[0]) + parseInt(collect[1]) <= offset || parseInt(collect[0]) + parseInt(collect[1]) >= offset) && word.toString().trim() == collect[3].toString().trim());
                });

                var itemDeleteword = temparray.filter(function (collect) {
                    return ((parseInt(collect[0]) + parseInt(collect[1]) <= offset || parseInt(collect[0]) + parseInt(collect[1]) >= offset) && word.toString().trim() == collect[3].toString().trim());
                });
                if (itemDeleteBackSpaceword.length == 0 && itemDeleteword.length == 0)
                {
                    var newUser = [];
                    newUser.push(finalupdatedword[k][0], finalupdatedword[k][1], finalupdatedword[k][2], finalupdatedword[k][3], finalupdatedword[k][4]);
                    listofwordschangedFinalMain.push(newUser);
                }
                else {

                    //updated word delete
                    //not need to add becuase aleady in deleted array items
//                    var newUser = [];
//                    newUser.push(finalupdatedword[k][0], finalupdatedword[k][1], finalupdatedword[k][2], finalupdatedword[k][3], "Delete");
//                    listofwordschangedFinalMain.push(newUser);
                }
            }
        }

//        for (var k = 0; k <= listofwordschangedFinalMain.length - 1; k++)
//        {
//            var id = parseInt(k) + 1;
//            console.log("Final :-  " + id + " " + listofwordschangedFinalMain[k][0], listofwordschangedFinalMain[k][1], listofwordschangedFinalMain[k][2], listofwordschangedFinalMain[k][3], listofwordschangedFinalMain[k][4]);
//
//        }

        if (desc != "")
        {
            $.ajax({
                type: 'POST',
                url: '<?php echo admin_url('admin-ajax.php'); ?>',
                method: "post",
//                cache: false,
//                contentType: false,
//                processData: false,
                //data: data,
                data: {
                    action: 'submited_doc_by_proofreader',
                    doc_id: docid,
                    userid: fk_proofreader_id,
                    data2: listofwordschangedFinalMain,
                    word_desc: desc,
                    fk_cust_id: fk_cust_id,
                    fk_main_doc_id: fk_main_doc_id,
                    fk_sub_doc_id: fk_sub_doc_id

                },
                type: 'POST',
                        success: function (data) {
                            console.log(data);
                            if (data == 'error')
                            {
                                $("#btnFinishedMySection").css("display", "");
                                $("#btnLooksPerfect").css("display", "none");
                                $("#txt_area_upload_doc").attr("contenteditable", "true");
                                $("#txt_area_upload_doc").removeClass("proofreaderdesc");
                                alert(data);
                            }
                            else
                            {


                                //alert double check


                                $.ajax({
                                    type: 'POST',
                                    url: '<?php echo admin_url('admin-ajax.php'); ?>',
                                    method: "post",
                                    data: {
                                        action: 'getDoubleCheckDesc'

                                    },
                                    type: 'POST',
                                            success: function (data1) {
                                                var res = jQuery.parseJSON(data1);
                                                $('#pop_start').css("display", "none");
                                                if (data1.trim() == "0")
                                                {
                                                    $("#btnFinishedMySection").css("display", "");
                                                    $("#btnLooksPerfect").css("display", "none");
                                                    $("#txt_area_upload_doc").attr("contenteditable", "true");
                                                    var url = '<?php echo get_page_link(810); ?>';
                                                    window.location.href = url;

                                                }
                                                else
                                                {
                                                    $('#pop_start').fadeIn('slow');
                                                    console.log(res[0][0].doc_desc);
                                                    $("#doublcheck_section").append(res[0][0].doc_desc);
                                                    $("#doublcheck_section").attr("data-id", res[0][0].fk_doc_details_id);
                                                    $("#btnFinishedMySection").css("display", "none");
                                                    $("#btnLooksPerfect").css("display", "");
                                                    $("#txt_area_upload_doc").attr("contenteditable", "false");
                                                    $("#txt_area_upload_doc").addClass("proofreaderdesc");
                                                    $("#doublcheck_section").attr("contenteditable", "true");
                                                }


                                            },
                                    error: function (jqXHR, textStatus, errorThrown) {
                                        //   document.getElementById("txt_area_upload_doc").contentEditable = true;
                                        alert(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                                    }
                                });
                                //alert(data);
                            }
                        },
                error: function (jqXHR, textStatus, errorThrown) {
                    //   document.getElementById("txt_area_upload_doc").contentEditable = true;
                    alert(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                }
            });
            return false;
        }
        else
        {
            alert('no data found');
        }
    });

    function getBackSpaceDeleteWords(word, offset, length)
    {

        var desc = $("#txt_area_upload_doc").text();

        var NewOffset = (parseInt(offset));
        var prevoffset = parseInt(NewOffset) - 1;
        var nextoffset = parseInt(NewOffset);
        var wrdFirstchar = word.charAt(0);
        var wrdLastchar = word.slice(-1);
        if (wrdFirstchar.toString().trim() == "" && wrdLastchar.toString().trim() == "")
        {
            return word + "^" + offset + "^" + length + "^Complete";
        }
        else if (length == 1)
        {
            wrdFirstchar = desc.substr(prevoffset, 1);
            wrdLastchar = desc.substr(nextoffset, 1);
            if (wrdFirstchar.toString().trim() == "" && wrdLastchar.toString().trim() == "")
            {
                return word + "^" + offset + "^" + length + "^Complete";
            }
            else if (wrdFirstchar.toString().trim() == "")
            {
                var wrd = "";
                offset = offset;
                while (wrdLastchar.toString().trim() != "")
                {
                    wrd = wrd + wrdLastchar;
                    nextoffset = parseInt(nextoffset) + 1;
                    wrdLastchar = desc.substr(nextoffset, 1);

                }
                length = (wrd.length);
                return wrd + "^" + offset + "^" + length;
            }
            else if (wrdLastchar.toString().trim() == "")
            {
                var wrd = "";

                while (wrdFirstchar.toString().trim() != "")
                {
                    wrd = wrdFirstchar + wrd;
                    prevoffset = parseInt(prevoffset) - 1;
                    wrdFirstchar = desc.substr(prevoffset, 1);

                }
                offset = prevoffset;
                length = (wrd.length);
                return wrd + "^" + offset + "^" + length;

            }
            else
            {
                var wrdPrevPart = "";
                var wrdNextPart = "";

                wrdFirstchar = desc.substr(prevoffset, 1);
                wrdPrevPart = wrdFirstchar + wrdPrevPart;
                while (wrdFirstchar.toString().trim() != "")
                {

                    prevoffset = parseInt(prevoffset) - 1;
                    wrdFirstchar = desc.substr(prevoffset, 1);
                    wrdPrevPart = wrdFirstchar + wrdPrevPart;

                }
                offset = prevoffset;
                wrdLastchar = desc.substr(nextoffset, 1);
                wrdNextPart = wrdNextPart + wrdLastchar;
                while (wrdLastchar.toString().trim() != "")
                {

                    nextoffset = parseInt(nextoffset) + 1;
                    wrdLastchar = desc.substr(nextoffset, 1);
                    wrdNextPart = wrdNextPart + wrdLastchar;

                }
                length = (parseInt(wrdPrevPart.length) + parseInt(wrdNextPart.length));
                return wrdPrevPart + wrdNextPart + "^" + offset + "^" + length;
            }


        }
        else
        {
            //length greater than

            wrdFirstchar = desc.substr(prevoffset, 1);
            wrdLastchar = desc.substr(nextoffset, 1);
            if (wrdFirstchar.toString().trim() == "" && wrdLastchar.toString().trim() == "")
            {
                return word + "^" + offset + "^" + length + "^Complete";
            }
            else if (wrdFirstchar.toString().trim() == "")
            {
                var wrd = "";
                offset = offset;
                while (wrdLastchar.toString().trim() != "")
                {
                    wrd = wrd + wrdLastchar;
                    nextoffset = parseInt(nextoffset) + 1;
                    wrdLastchar = desc.substr(nextoffset, 1);

                }
                length = (wrd.length);
                return wrd + "^" + offset + "^" + length;
            }
            else if (wrdLastchar.toString().trim() == "")
            {
                var wrd = "";

                while (wrdFirstchar.toString().trim() != "")
                {
                    wrd = wrdFirstchar + wrd;
                    prevoffset = parseInt(prevoffset) - 1;
                    wrdFirstchar = desc.substr(prevoffset, 1);

                }
                offset = prevoffset;
                length = (wrd.length);
                return wrd + "^" + offset + "^" + length;
            }
            else
            {
                var wrdPrevPart = "";
                var wrdNextPart = "";

                wrdFirstchar = desc.substr(prevoffset, 1);
                wrdPrevPart = wrdFirstchar + wrdPrevPart;
                while (wrdFirstchar.toString().trim() != "")
                {

                    prevoffset = parseInt(prevoffset) - 1;
                    wrdFirstchar = desc.substr(prevoffset, 1);
                    wrdPrevPart = wrdFirstchar + wrdPrevPart;
                }
                offset = prevoffset;
                wrdLastchar = desc.substr(nextoffset, 1);
                wrdNextPart = wrdNextPart + wrdLastchar;
                while (wrdLastchar.toString().trim() != "")
                {

                    nextoffset = parseInt(nextoffset) + 1;
                    wrdLastchar = desc.substr(nextoffset, 1);
                    wrdNextPart = wrdNextPart + wrdLastchar;

                }
                length = (parseInt(wrdPrevPart.length) + parseInt(wrdNextPart.length));
                return wrdPrevPart + wrdNextPart + "^" + offset + "^" + length;
            }



        }

    }

    function GetOldWordMiddleSpace(docdetails, offset, length, lengthupdated)
    {

        var newstring = docdetails.trim().substr(offset + length, 1);

        while (newstring.trim() != "") {
            offset = parseInt(offset) + 1;
            newstring = docdetails.substr(offset, 1);
            // length++;
        }

        var firstchar = newstring.substr(0, 1);
        var lastchar = newstring.slice(-1);
        offset = parseInt(offset) + 1;
        newstring = docdetails.trim().substr(offset, 1);
        if (firstchar.trim() != "" && lastchar.trim() != "")
        {
            console.log("both blank");
        } else {
            if (firstchar.trim() != "")
            {
                while (newstring.trim() != "") {
                    offset = parseInt(offset) + 1;
                    newstring = docdetails.substr(offset, 1);
                }
            }
            offset = parseInt(offset) + 1;
            newstring = docdetails.trim().substring(offset, length);
            var lastchar = newstring.slice(-1);
            if (lastchar.trim() != "")
            {
                while (newstring.trim() != "") {
                    offset = parseInt(offset) + 1;
                    newstring = docdetails.substr(offset, 1);
                }
            }
        }
        return docdetails.substr(offset, length + 1) + "^" + offset + "^";
    }


    function getOldstringwhenupdatedFinal(docdetails, offsetold)
    {
        var val = docdetails;

        //  console.log("String.fromCharCode(e.which).toLowerCase() " + String.fromCharCode(e.which).toLowerCase());
        // updatednew = String.fromCharCode(e.which).toLowerCase();
        var oldstring = "";
        var length = 1;
        var offset = offsetold;
        var updatednew = "";

        var tmpstartpostions = parseInt(offset) - 1;
        var tmpstartstring = val.highLightAt(tmpstartpostions);

        var removestart = 0;
        var preveiousstrings = "";
        while (tmpstartstring.trim().length > 0)
        {
            if (tmpstartstring.trim().length != 0)
            {
                tmpstartpostions = parseInt(tmpstartpostions) - 1;
                preveiousstrings += tmpstartstring;
            }
            removestart++;
            tmpstartstring = val.highLightAt(tmpstartpostions);


        }


        oldstring = "";
        updatednew = "";
        var tmpendpostions = parseInt(offset) - removestart;
        offset = tmpendpostions - 1;
        var tmpendstring = val.highLightAt(tmpendpostions);

        preveiousstrings += tmpendstring;
        while (tmpendstring.trim().length > 0)
        {
            tmpendstring = val.highLightAt(tmpendpostions);
            // alert("end" + (tmpendstring));

            tmpendpostions = parseInt(tmpendpostions) + 1;

            oldstring += tmpendstring;
            updatednew += tmpendstring;



        }

        // alert(oldstring.length);
        length = oldstring.length - 1;
        //added 30-march-2017
        if (length < 0)
        {
            length = 1;
            oldstring = "";

        }
        return oldstring;
    }
</script>