<?php
get_header();

if(!isset($_REQUEST['level'])){
?>
<section>
    <div class="breadcum">
        <div class="container">
            <div class="page_title">
                <?php the_title('<h1>', '</h1>'); ?>
            </div>
        </div>
    </div>
</section>
<?php }?>
<section>
    <div class="term_service">
        <div class="container ">           
            <?php
            if (have_posts()) :
                while (have_posts()) : the_post();
                    the_content();
                endwhile;
            endif;
            ?>
        </div>
    </div>
</section>
<?php
if(!isset($_REQUEST['level'])){
get_footer();
}
