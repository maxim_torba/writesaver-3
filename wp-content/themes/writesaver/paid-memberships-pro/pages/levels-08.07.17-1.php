<?php
global $wpdb, $pmpro_msg, $pmpro_msgt, $current_user;

$pmpro_levels = pmpro_getAllLevels(false, true);
//echo '<pre>';
//print_r($pmpro_levels);exit;
$pmpro_level_order = pmpro_getOption('level_order');

if (!empty($pmpro_level_order)) {
    $order = explode(',', $pmpro_level_order);

    //reorder array
    $reordered_levels = array();
    foreach ($order as $level_id) {
        foreach ($pmpro_levels as $key => $level) {
            if ($level_id == $level->id)
                $reordered_levels[] = $pmpro_levels[$key];
        }
    }

    $pmpro_levels = $reordered_levels;
}

$pmpro_levels = apply_filters("pmpro_levels_array", $pmpro_levels);

if ($pmpro_msg) {
    ?>
    <div class="pmpro_message <?php echo $pmpro_msgt ?>"><?php echo $pmpro_msg ?></div>
    <?php
}
?>
<div class="all_plans pmpro_checkout"  id="pmpro_levels_table">
    <div class="row">
        <?php
        $count = 0;
        foreach ($pmpro_levels as $level) {
            if (isset($current_user->membership_level->ID))
                $current_level = ($current_user->membership_level->ID == $level->id);
            else
                $current_level = false;
            if ($level->most_popular == 1)
                $class = "most_popular";
            else
                $class = "";
            ?>
            <div class="col-md-3 col-sm-6 <?php if ($count++ % 2 == 0) { ?>odd<?php } ?><?php if ($current_level == $level) { ?> active<?php } ?>">
                <div class="plan_box <?php echo $class; ?>">
                    <div class="plan_price">
                        <span class="price">
                            <?php
                            if (pmpro_isLevelFree($level))
                                $cost_text = "<strong>" . __("Free", "pmpro") . "</strong>";
                            else
                                $cost_text = pmpro_getLevelCost($level, true, true);
                            $expiration_text = pmpro_getLevelExpiration($level);
                            if (!empty($cost_text))
                                echo $cost_text;
                            elseif (!empty($expiration_text))
                                echo $expiration_text;
                            ?>
                        </span>
                        <span><?php echo $level->name; ?></span>
                    </div>
                    <div class="plan_details">
                        <div class="plan_details_box">
                            <div class="plan_detail_txt_main">
                                <div class="plan_detail_txt">
                                    <p><?php echo $level->plan_words; ?> words</p>
                                    <?php echo $level->description; ?>
                                </div>                                                
                            </div>
                            <div class="btn_blue">

                                <?php if (empty($current_user->membership_level->ID)) { ?>
                                    <a class=" pmpro_btn-select btn_sky class_bill_method" id="<?php echo $level->id;?>" href="javascript:void(0);"><?php _e('Purchase now', 'pmpro'); ?></a>
                                <?php } elseif (!$current_level) { ?>                	
                                    <a class=" pmpro_btn-select btn_sky class_bill_method" id="<?php echo $level->id;?>" href="javascript:void(0);"><?php _e('Purchase now', 'pmpro'); ?></a>
                                <?php } elseif ($current_level) { ?>      

                                    <?php
                                    //if it's a one-time-payment level, offer a link to renew				
                                    if (pmpro_isLevelExpiringSoon($current_user->membership_level) && $current_user->membership_level->allow_signups) {
                                        ?>
                                        <a class=" pmpro_btn-select btn_sky class_bill_method" id="<?php echo $level->id;?>" href="javascript:void(0);"><?php _e('Upgrade Plan', 'pmpro'); ?></a>
                                        <?php
                                    } else {
                                        ?>
                                        <a class=" disabled btn_sky class_bill_method" id="<?php echo $level->id;?>" href="javascript:void(0);"><?php _e('Purchase now', 'pmpro'); ?></a>
                                        <?php
                                    }
                                    ?>

                                <?php } ?>                               
                            </div>
                        </div>
                    </div>
                    <?php if ($level->most_popular == 1): ?>
                        <div class="most_popular_ad">
                            <span> Most popular</span>
                        </div>
                    <?php endif; ?>
                </div> 
            </div>
            <?php
        }
        ?>
		<script>
		// new code
		jQuery(document).ready(function ($) {
			
			$('.class_bill_method').click(function(){
				
				var level_id = $(this).attr('id');
				$('#hid_mamu_level_id').val(level_id);
				
				$('#payment_option_modal').fadeIn();
				$('#payment_option_modal').removeClass('open');
				
				return false;
			});
			$('#openBtn1').click(function(){
				
				var pay_type = $(this).attr('data-card-type');
				var level_id = $('#hid_mamu_level_id').val();
				var mamu_url = $('#hid_mamu_url').val();
				
				$('#frame_plan1').attr('src', mamu_url+"level="+level_id+"&i=i&pay_type="+pay_type)
				
				$('#myModal1').fadeOut();
				$('#myModal1').addClass('open');
				$('#myModal1').css('display', 'block');
				
				return false;
			});
			
			$('#openBtn2').click(function(){
				
				var pay_type = $(this).attr('data-card-type');
				var level_id = $('#hid_mamu_level_id').val();
				var mamu_url = $('#hid_mamu_url').val();
				
				$('#frame_plan1').attr('src', mamu_url+"level="+level_id+"&i=i&pay_type="+pay_type)
				
				$('#myModal1').fadeOut();
				$('#myModal1').addClass('open');
				$('#myModal1').css('display', 'block');
				
				return false;
			});
			
			$('#payment_option_modal a.ppmodal .fa-remove').click(function (e) {
				e.stopPropagation();
				$('#payment_option_modal').fadeOut();
				$('#payment_option_modal').removeClass('open');
			});
			
			$('#myModal1 a.ppmodal .fa-remove').click(function (e) {
				$('#myModal1').fadeIn();
				$('#myModal1').removeClass('open');
				$('#myModal1').css('display', 'none');
			});
		});
		function goToPaypal(){
			
			var paypal_id = $('#paypal_id').val().trim();
			if(paypal_id == ""){
				$('#paypal_id').focus();
				return false;
			}
			$.post("<?php echo get_template_directory_uri();?>/ajax.php",{"choice":"update_paypal","paypal_id":paypal_id},function(result){
				
				var tmp = result.split('~');
				if(tmp[0] == "yes"){
					
					var pay_type = $(this).attr('data-card-type');
					var level_id = $('#hid_mamu_level_id').val();
					var mamu_url = $('#hid_mamu_url').val();
					
					$('#frame_plan1').attr('src', mamu_url+"level="+level_id+"&i=i&pay_type="+pay_type)
					
					$('#myModal1').fadeOut();
					$('#myModal1').addClass('open');
					$('#myModal1').css('display', 'block');
					
					return false;
				}
			});
		}
		</script>
        
        <?php
		$user_id = get_current_user_id();
		$paypal_result = $wpdb->get_results("SELECT * FROM tbl_customer_general_info WHERE fk_customer_id = ".$user_id." LIMIT 1 ");
		?>
        
        <!--Billing Method-->
        <input type="hidden" id="hid_mamu_level_id" value="" />
        <input type="hidden" id="hid_mamu_url" value="<?php echo get_site_url();?>/membership-account/membership-checkout/?" />
        <input type="hidden" id="hid_mamu_pay_type" value="" />
        <div id="payment_option_modal" class="pop_overlay" style="display: none;">
            <div class="pop_main" style="bottom:30%;">
                <div class="pop_head">
                    <a href="javascript:void(0);" data-dismiss="modal" type="button" class="ppmodal" ><i class="fa fa-remove"  aria-hidden="true"></i></a>
                </div>
                <div class="pop_body">
                    <div class="setting_right">
                        <div class="field_title">
                            <h4>Billing Methods</h4>
                        </div>
                        <div class="">
                            <div class="billing_table_responsive">
                                <div class="billing_table">
                                    <div class="customer_billing_methods">
                                        <div class="billing_image">
                                            <img src="<?php echo get_template_directory_uri() ?>/images/paypal.png" alt="paypal">
                                        </div>
                                        <div class="billing_type">
                                            <p>Paypal</p>
                                        </div>
                                        <div class="bill_popup" style="width:50%;">
                                        	<?php if($paypal_result[0]->paypal_id == ""){?>
                                            <a href="#" role="button" data-toggle="modal" data-target="#paypal_modal" data-card-type="paypal" class="paypal_pop btn_sky">Set up</a>
                                            <?php }else{?>
                                            <a href="<?php echo home_url();?>/paypal-process/" role="button" data-toggle="modal" data-card-type="paypal" class="paypal_pop btn_sky">Proceed</a>
                                            <?php }?>
                                        </div>
                                    </div>
                                    <div class="customer_billing_methods">
                                        <div class="billing_image">
                                            <img src="<?php echo get_template_directory_uri() ?>/images/credit_debit.png" alt="paypal">
                                        </div>
                                        <div class="billing_type">
                                            <p>Credit or debit card</p>
                                        </div>
                                        <div class="bill_popup">
                                            <a href="javascript:void(0);" class="btn_sky pop_btn" data-toggle="modal" data-card-type="card" id="openBtn1">Set up</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!--Paypal Method-->
		<div id="paypal_modal" class="pop_overlay" style="display: none;">
			<div class="pop_main" style="bottom:30%;">
				<div class="pop_head">
					<a href="javascript:void(0);" data-dismiss="modal" type="button" ><i class="fa fa-remove"  aria-hidden="true"></i></a>
				</div>
				<div class="pop_body">
					<div class="page_title">
						<h2>Add a Paypal</h2>
						<h4>Payment information</h4>
					</div>
					<div class="pop_content">
						
						<form method="post" action="" id="cust_paypal" autocomplete="off">
							<div class="row">
								<div class="col-sm-offset-2 col-sm-8">
									<input type="email" class="contact_block" name="paypal_id" id="paypal_id" value="<?php echo $paypal_result[0]->paypal_id; ?>" maxlength="100"  placeholder="Paypal Id*">
								</div>
								<div class="buttons col-sm-12" style="margin-top:2%;">
									<input type="button" name="add_paypal" id="add_paypal" value="Update" class="btn_sky" style="float:none;" onclick="goToPaypal();">
									<input data-dismiss="modal" type="button" id="close_paypal_model" value="Cancel" class="btn_sky">
									<input type="hidden" name="paypal_extra_words" id="paypal_extra_words" />
									<input type="hidden" name="paypal_pay_amount" id="paypal_pay_amount" />
								</div>
								<div class="paypal_msg"></div>
							</div>
						</form>
						
					</div>
				</div>
			</div>
		</div>
        
        <!-- Plan Details Modal -->
        <div id="myModal1" class="pop_overlay open" style="display: none;">
            <div class="pop_main" style="height:550px;bottom:30%; width:430px;">
                <div class="pop_head" style="min-height:0;">
                    <a href="javascript:void(0);" data-dismiss="modal" type="button" class="ppmodal" ><i class="fa fa-remove"  aria-hidden="true"></i></a>
                </div>
                <div class="pop_body" style="margin-left:-30px">
                    <iframe id="frame_plan1" src="<?php echo get_site_url();?>/membership-account/membership-checkout/?level=1" style="width:430px; height:110%; border:none;"></iframe>
                </div>
            </div>
        </div>
        <!-- /Plan Details #myModal -->
    </div>
</div>
