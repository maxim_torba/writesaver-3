<?php
global $wpdb, $pmpro_msg, $pmpro_msgt, $current_user;

$pmpro_levels = pmpro_getAllLevels(false, true);
$pmpro_level_order = pmpro_getOption('level_order');

if (!empty($pmpro_level_order)) {
    $order = explode(',', $pmpro_level_order);

    //reorder array
    $reordered_levels = array();
    foreach ($order as $level_id) {
        foreach ($pmpro_levels as $key => $level) {
            if ($level_id == $level->id)
                $reordered_levels[] = $pmpro_levels[$key];
        }
    }

    $pmpro_levels = $reordered_levels;
}

$pmpro_levels = apply_filters("pmpro_levels_array", $pmpro_levels);

if ($pmpro_msg) {
    ?>
    <div class="pmpro_message <?php echo $pmpro_msgt ?>"><?php echo $pmpro_msg ?></div>
    <?php
}
?>
<div class="all_plans pmpro_checkout"  id="pmpro_levels_table">
    <div class="row">
        <?php
        $count = 0;
        foreach ($pmpro_levels as $level) {
            if (isset($current_user->membership_level->ID))
                $current_level = ($current_user->membership_level->ID == $level->id);
            else
                $current_level = false;
            if ($level->most_popular == 1)
                $class = "most_popular";
            else
                $class = "";
            ?>
            <div class="col-md-3 col-sm-6 <?php if ($count++ % 2 == 0) { ?>odd<?php } ?><?php if ($current_level == $level) { ?> active<?php } ?>">
                <div class="plan_box <?php echo $class; ?>">
                    <div class="plan_price">
                        <span class="price">  
                            <?php
                            if (pmpro_isLevelFree($level))
                                $cost_text = "<strong>" . __("Free", "pmpro") . "</strong>";
                            else
                                $cost_text = pmpro_getLevelCost($level, true, true);
                            $expiration_text = pmpro_getLevelExpiration($level);
                            if (!empty($cost_text))
                                echo $cost_text;
                            elseif (!empty($expiration_text))
                                echo $expiration_text;
                            ?>
                        </span>
                        <span><?php echo $level->name; ?></span>
                    </div>
                    <div class="plan_details">
                        <div class="plan_details_box">
                            <div class="plan_detail_txt_main">
                                <div class="plan_detail_txt">
                                    <p><?php echo $level->plan_words; ?> free words </br>
                                    <span style="font-size:12px"><em>$<?php echo $level->price_per_additional_word; ?> per additional word</em></span></p>
                                    <p><?php echo $level->description; ?></p>
                                </div>                                                
                            </div>
                            <div class="btn_blue">

                                <?php if (empty($current_user->membership_level->ID)) { ?>
                                    <a class=" pmpro_btn-select btn_sky" href="<?php echo pmpro_url("checkout", "?level=" . $level->id, "https") ?>"><?php _e('Purchase now', 'pmpro'); ?></a>
                                <?php } elseif (!$current_level) { ?>                	
                                    <a class=" pmpro_btn-select btn_sky"  href="<?php echo pmpro_url("checkout", "?level=" . $level->id, "https") ?>"><?php _e('Purchase now', 'pmpro'); ?></a>
                                <?php } elseif ($current_level) { ?>      

                                    <?php
                                    //if it's a one-time-payment level, offer a link to renew				
                                    if (pmpro_isLevelExpiringSoon($current_user->membership_level) && $current_user->membership_level->allow_signups) {
                                        ?>
                                        <a class=" pmpro_btn-select btn_sky" href="<?php echo pmpro_url("checkout", "?level=" . $level->id, "https") ?>"><?php _e('Upgrade Plan', 'pmpro'); ?></a>
                                        <?php
                                    } else {
                                        ?>
                                        <a class=" disabled btn_sky" href="<?php echo pmpro_url("checkout", "?level=" . $level->id, "https") ?>"><?php _e('Purchase now', 'pmpro'); ?></a>
                                        <?php
                                    }
                                    ?>

                                <?php } ?>                               
                            </div>
                        </div>
                    </div>
                    <?php if ($level->most_popular == 1): ?>
                        <div class="most_popular_ad">
                            <span> Most popular</span>
                        </div>
                    <?php endif; ?>
                </div> 
            </div>
            <?php
        }
        ?>

    </div>
</div>
