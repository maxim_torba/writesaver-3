<?php
/**
*  Example of use of HTML to docx converter
*/

// Load the files we need:
require_once 'phpword/PHPWord.php';
require_once 'simplehtmldom/simple_html_dom.php';
require_once 'htmltodocx_converter/h2d_htmlconverter.php';
require_once 'example_files/styles.inc';
require_once 'finediff.php';
// Functions to support this example.
require_once 'documentation/support_functions.inc';
include '../../../../../wp-load.php';

//EVNE, DM
$html = '';
$origin_desc_text = '';
if(isset($_GET['doc']) && !empty($_GET['doc'])){
  global $wpdb;
  $main_doc_id = $_GET['doc'];
  $all_doc = $wpdb->get_results("SELECT * FROM tbl_proofreaded_doc_details WHERE fk_doc_main_id = $main_doc_id");
  $origin_desc = $wpdb->get_results("SELECT document_desc FROM wp_customer_document_details WHERE fk_doc_main_id = $main_doc_id");
  foreach ($origin_desc as $origin_desc_doc) {
    $origin_desc_text .= $origin_desc_doc->document_desc;
  }
  foreach ($all_doc as $doc) {
    $html .= $doc->double_doc_desc;
  }
}else{
  die('Provide main doc ID');
}
echo $origin_desc_text;
echo '<hr>';
echo $html;

    
$filename = 'doc.docx';
$html = str_replace('<p></p>', '', $html);
$html = preg_replace('/(<br>){2,}/', '', $html);
$html = preg_replace('/(\s){2,}/', '', $html);
$html = preg_replace('/(<br>){1}/', '', $html);

$origin_desc_text = str_replace('<p></p>', '', $origin_desc_text);
$origin_desc_text = preg_replace('/(<br>){2,}/', '', $origin_desc_text);
$origin_desc_text = preg_replace('/(\s){2,}/', '', $origin_desc_text);
$origin_desc_text = preg_replace('/(<br>){1}/', '', $origin_desc_text);

$html = str_replace('paragraph', '$CHANGE$', $html);

require_once '../../../../../phpdocx/classes/CreateDocx.inc';
$docx = new CreateDocx();
$docx->embedHTML($html);

$docx->createDocx($filename);



//require_once '../../../../../phpdocx/classes/DocxUtilities.inc';
//$changePattern = 'test document';
//$newDocx = new DocxUtilities();
//$options = array(
//    'document' => true,
//    'endnotes' => true,
//    'comments' => true,
//    'headersAndFooters' => true,
//    'footnotes' => true,
//);
//
//$newDocx->searchAndReplace($filename, $filename, $changePattern, '$CHANGE$', $options);
//require_once '../../../../../phpdocx/classes/CreateDocx.inc';
require_once '../../../../../phpdocx/classes/CreateDocxFromTemplate.inc';
$docx = new CreateDocxFromTemplate($filename);
//create the Word fragment that is going to replace the variable
$comment = new WordFragment($docx, 'document');
$comment->addComment(
    array(
        'textDocument' => 'comment',
        'textComment' => 'changed: '. $changePattern,
        'initials' => 'WS',
        'author' => 'Writesaver.co',
        'date' => '09 OCT 2017'
    )
);
//create an image fragment
$docx->replaceVariableByWordFragment(array('CHANGE' => $comment), array('type' => 'inline'));

$docx->createDocx($filename);
echo '<br><a href="https://staging.writesaver.co/wp-content/themes/writesaver/inc/htmltodocx/doc.docx">Download docx</a>';

//echo $html;
// HTML fragment we want to parse:
//$html = file_get_contents('example_files/example_html.html');
// $html = file_get_contents('test/table.html');

// New Word Document:
$phpword_object = new PHPWord();
$section = $phpword_object->createSection();

// HTML Dom object:
$html_dom = new simple_html_dom();
$html_dom->load('<html><body>' . $html . '</body></html>');
// Note, we needed to nest the html in a couple of dummy elements.

// Create the dom array of elements which we are going to work on:
$html_dom_array = $html_dom->find('html',0)->children();

// We need this for setting base_root and base_path in the initial_state array
// (below). We are using a function here (derived from Drupal) to create these
// paths automatically - you may want to do something different in your
// implementation. This function is in the included file
// documentation/support_functions.inc.
$paths = htmltodocx_paths();

// Provide some initial settings:
$initial_state = array(
  // Required parameters:
  'phpword_object' => &$phpword_object, // Must be passed by reference.
  // 'base_root' => 'http://test.local', // Required for link elements - change it to your domain.
  // 'base_path' => '/htmltodocx/documentation/', // Path from base_root to whatever url your links are relative to.
  'base_root' => $paths['base_root'],
  'base_path' => $paths['base_path'],
  // Optional parameters - showing the defaults if you don't set anything:
  'current_style' => array('size' => '11'), // The PHPWord style on the top element - may be inherited by descendent elements.
  'parents' => array(0 => 'body'), // Our parent is body.
  'list_depth' => 0, // This is the current depth of any current list.
  'context' => 'section', // Possible values - section, footer or header.
  'pseudo_list' => TRUE, // NOTE: Word lists not yet supported (TRUE is the only option at present).
  'pseudo_list_indicator_font_name' => 'Wingdings', // Bullet indicator font.
  'pseudo_list_indicator_font_size' => '7', // Bullet indicator size.
  'pseudo_list_indicator_character' => 'l ', // Gives a circle bullet point with wingdings.
  'table_allowed' => TRUE, // Note, if you are adding this html into a PHPWord table you should set this to FALSE: tables cannot be nested in PHPWord.
  'treat_div_as_paragraph' => TRUE, // If set to TRUE, each new div will trigger a new line in the Word document.

  // Optional - no default:
  'style_sheet' => htmltodocx_styles_example(), // This is an array (the "style sheet") - returned by htmltodocx_styles_example() here (in styles.inc) - see this function for an example of how to construct this array.
  );

// Convert the HTML and put it into the PHPWord object
htmltodocx_insert_html($section, $html_dom_array[0]->nodes, $initial_state);


// Clear the HTML dom object:
$html_dom->clear();
unset($html_dom);
// Save File
$h2d_file_uri = tempnam('', 'htd');
$objWriter = PHPWord_IOFactory::createWriter($phpword_object, 'Word2007');
//$objWriter->save('document-'.$main_doc_id);

//
//// Download the file:
//header('Content-Description: File Transfer');
//header('Content-Type: application/octet-stream');
//header('Content-Disposition: attachment; filename=document.docx');
//header('Content-Transfer-Encoding: binary');
//header('Expires: 0');
//header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
//header('Pragma: public');
//header('Content-Length: ' . filesize($h2d_file_uri));
//ob_clean();
//flush();
//$status = readfile($h2d_file_uri);
//unlink($h2d_file_uri);
//exit;