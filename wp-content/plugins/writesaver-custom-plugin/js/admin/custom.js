jQuery('document').ready(function () {
    jQuery(window).on('load resize', function () {
        var editor_width = jQuery(".hidden_scroll").width();
        var editor_tot_width = jQuery(".hidden_scroll").width() + 16.5;
        jQuery('.hidden_scroll').css('width', editor_width);
        jQuery('.parentscrollcontents').css('width', editor_tot_width);
    });

    jQuery(".changeable").focusout(function () {
        var element = jQuery(this);
        if (!element.text().replace(" ", "").length) {
            element.empty();
        }
    });

    jQuery('.close_tab .category_btn').on('click', function (evnt) {

        if (jQuery('.close_tab #links').hasClass('open')) {
            jQuery('.close_tab #links').removeClass('open').hide();
        } else {
            jQuery('.close_tab #links').addClass('open').show();
        }
    });

    jQuery('.only_alpha').keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z]+jQuery");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        } else {
            e.preventDefault();
            return false;
        }
    });

    jQuery(".only_num").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

//    setTimeout(function () {
//        jQuery('#subscrib_pop').fadeIn('slow');
//        jQuery('body').removeClass('pop_open');
//    }, 4000);
//    jQuery('.pop_head a').click(function () {
//        jQuery('#subscrib_pop').fadeOut('slow');
//        jQuery('body').removeClass('pop_open');
//    });
    jQuery('.create_new_doc button').click(function () {
        jQuery('#pop_start').fadeIn('slow');
    });
    jQuery('.pop_head a').click(function () {
        jQuery('#pop_start').fadeOut('slow');
        jQuery('#pop_start_Confirm').fadeOut('slow');

    });

    jQuery('.delete_track a').click(function () {
        jQuery('#pop_start_deleted_words').fadeIn('slow');
    });

    jQuery('#pop_start_deleted_words .pop_head a').click(function () {
        jQuery('#pop_start_deleted_words').fadeOut('slow');
    });

    jQuery('.pop_head a').click(function () {
        jQuery('#pop_start').fadeOut('slow');
    });
    jQuery('.btn_blue a').click(function () {
        //jQuery('#pop_start').fadeIn('slow');
    });
    //Edit password popup
    jQuery('.edit_link a.chg_pass').click(function () {
        jQuery('#pass_modal').fadeIn('slow');
    });
    jQuery('#pass_modal .pop_head a').click(function () {
        jQuery('#pass_modal').fadeOut('slow');
    });

    jQuery('#section1 a.instant_video_btn').on('click', function (e) {
        e.preventDefault();
        jQuery(this).hide();
        jQuery('#section1 .wpb_video_widget.instant_video iframe')[0].src += "&autoplay=1";

        return false;
    });

    jQuery('#about_writesaver a.instant_video_btn').on('click', function (ev) {
        debugger;
        jQuery(this).hide();
        jQuery('#about_writesaver .wpb_video_widget.about_writesaver_video  iframe')[0].src += "&autoplay=1";
        ev.preventDefault();
    });

    jQuery('.check p').on({
        mouseenter: function () {
            jQuery(".popup_text").remove();
            jQuery('.hidden_scroll.check').append('<span class="popup_text">Proofread by other proofreader</span>');
            var p = jQuery(this);
            var offset = p.offset();
            var offset_top = offset.top;
            if (offset_top < 0) {
                jQuery('.popup_text').css('top', 90);
            } else {
                jQuery('.popup_text').css('top', offset.top / 3);
            }

        },
        mouseleave: function () {
            jQuery(".popup_text").remove();
        }
    });
    var window_height = jQuery(window).height();
    var tot_height = window_height - jQuery('#wpadminbar').outerHeight() - jQuery('#wpfooter').outerHeight();    
    jQuery('#wpbody').css('min-height',tot_height);
});
jQuery(window).on('load resize',function (){
    var window_height = jQuery(window).height();
    var tot_height = window_height - jQuery('#wpadminbar').outerHeight() - jQuery('#wpfooter').outerHeight();    
    jQuery('#wpbody').css('min-height',tot_height);
});
if (jQuery(window).width() >= 768) {
    var window_height = jQuery(window).height();
    jQuery('.top_section').css('height', window_height);
    jQuery('.instant').css('height', window_height);
    jQuery('.highlight').css('height', window_height);
    jQuery('.detection').css('height', window_height);
//jQuery('.home_proofreader').css('height', window_height);

    jQuery(window).bind("resize", function () {
        var window_height = jQuery(window).height();
        jQuery('.top_section').css('height', window_height);
        jQuery('.instant').css('height', window_height);
        jQuery('.highlight').css('height', window_height);
        jQuery('.detection').css('height', window_height);
//        jQuery('.home_proofreader').css('height', window_height);
    });
}




//Sticky Js
jQuery(window).scroll(function () {
    var bread_hei = jQuery('.breadcum').outerHeight();
    if (jQuery(window).scrollTop() > bread_hei) {
        jQuery('.basic .wrapper-sticky').addClass('sticky-active');
        jQuery('.basic .blog_category_sticky').addClass('sticky');
    } else {
        jQuery('.basic .wrapper-sticky').removeClass('sticky-active');
        jQuery('.basic .blog_category_sticky').removeClass('sticky');
    }
    if (jQuery('.header.fixed').length) {
        headr_height = jQuery('.header.fixed').outerHeight();
        jQuery('.about_sticky').hcSticky({
            top: headr_height
        });
        jQuery('.blog_category_sticky').hcSticky({
            top: headr_height
        });
    }
});
if (jQuery('.header.fixed').lengh) {
    jQuery(window).bind("resize", function () {
        jQuery('.about_sticky').hcSticky({
            top: headr_height
        });
        jQuery('.blog_category_sticky').hcSticky({
            top: headr_height
        });
    });
}

jQuery('.about_sticky ul li a[href*="#"]:not([href="#"])').click(function () {
    if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
        var target = jQuery(this.hash);
        var header_height = jQuery('header').outerHeight();
        target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            jQuery('html, body').animate({
                scrollTop: target.offset().top - 90
            }, 200);
            return;
        }
    }

});
jQuery('.about_sticky ul li ').on('click', function () {
    jQuery('.about_sticky ul li.active').removeClass('active');
    jQuery(this).addClass('active');
});




jQuery('.category_full_list #links  li').on('click', function () {

    jQuery('.blog_category_main .blog_listt:eq(' + eval(jQuery(this).index()) + ')').fadeIn("show").siblings().fadeOut();
});
jQuery('#links ').on('click', 'li', function () {
    jQuery('#links  li.active').removeClass('active');
    jQuery(this).addClass('active');
});

jQuery('.category_btn').on('click', function (evnt) {
    evnt.preventDefault();
    evnt.stopPropagation();
    if (jQuery('.category_full_list #links').hasClass('open')) {
        jQuery('.category_full_list #links').removeClass('open').hide();
    } else {
        jQuery('.category_full_list #links').addClass('open').show();
    }
});

if (jQuery(window).width() < 768) {
    jQuery(".category_full_list #links li").click(function () {
        jQuery('.category_full_list #links').hide();
    });
}

jQuery('.help_list #links  li').on('click', function () {
    jQuery('.help_center_blockmain .help_center_block:eq(' + eval(jQuery(this).index()) + ')').fadeIn("show").siblings().fadeOut();
});



jQuery(document).ready(function (e) {
    jQuery('.openre').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        jQuery(this).addClass('open');
        jQuery('.collapsible_content').slideDown('slow');
    });
    jQuery('.closer').click(function (evnt) {
        evnt.preventDefault();
        evnt.stopPropagation();
        if (jQuery('.openre').hasClass('open')) {
            jQuery('.openre').removeClass('open');
            jQuery('.collapsible_content').slideUp('slow');
        }
    });
    jQuery('.collapsible_content').click(function (et) {
        et.preventDefault();
        et.stopPropagation();
    });
    jQuery(document).click(function (ent) {
        ent.stopPropagation();
        jQuery('.openre').removeClass('open');
        jQuery('.collapsible_content').slideUp('slow');
    });

});
