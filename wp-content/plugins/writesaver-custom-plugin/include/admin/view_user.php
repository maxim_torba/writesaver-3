<?php
/*
 *  Writesaver View
 */
wp_enqueue_style('admin-custom-bootstrap', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/bootstrap.min.css', '', 'all');

wp_enqueue_style('admin-font-style', get_template_directory_uri() . '/css/font-awesome.css', '', '', 'all');
wp_enqueue_style('admin-custom-style', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/style.css', '', '', 'all');
wp_enqueue_style('admin-responsive-tab-style', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/responsive-tab.css', '', '', 'all');
wp_enqueue_style('admin-responsive-style', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/responsive.css', '', '', 'all');
wp_enqueue_style('admin-style_uv-style', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/style_uv.css', '', '', 'all');
wp_enqueue_script('admin-custom-js', WRITESAVER_CUSTOM_PLUGIN_URL . '/js/admin/jquery.min.js', array('jquery'), '', 'all');
wp_enqueue_script('admin-jquery-js', WRITESAVER_CUSTOM_PLUGIN_URL . '/js/admin/custom.js', array('jquery'), '', 'all');

wp_enqueue_script('admin-custom-bootstrap-js', WRITESAVER_CUSTOM_PLUGIN_URL . '/js/admin/bootstrap.min.js', array('jquery'), '', 'all');
global $wpdb;

$user_id = $_GET['user'];
if (empty($user_id)) {
    print('<script>window.location.href="users.php"</script>');
    exit;
}
$user_info = get_userdata($user_id);
$user_role = $user_info->roles[0];

$profile_pic = get_user_meta($user_id, 'profile_pic_url', true);
?>
<div class="load_overlay" id="loding">
    <img src="<?php echo get_template_directory_uri(); ?>/images/39.gif"/>
</div>
<section>
    <div class="breadcum">
        <div class="container">
            <div class="page_title">
                <h1><?php echo $user_info->first_name . ' ' . $user_info->last_name . ' (' . $user_role . ')'; ?></h1>
            </div>
        </div>
    </div>
</section>

<?php
if ($user_role == 'proofreader') {

    $user_tax = $wpdb->get_results("SELECT * FROM tbl_proofreader_general_details WHERE fk_proofreader_id = $user_id LIMIT 1");
    $result = $wpdb->get_results("SELECT * FROM wp_proofreader_notification_setting WHERE user_id = $user_id");
    $desktop_new_doc = "";
    $desktop_other_notification = "";
    $email_info_about_availability = "";
    $email_relevant_stories = "";

    if (count($result) > 0) {
        $desktop_new_doc = ($result[0]->desktop_new_doc == 1 ? "checked" : "");
        $desktop_other_notification = ($result[0]->desktop_other_notification == 1 ? "checked" : "");
        $email_info_about_availability = ($result[0]->email_info_about_availability == 1 ? "checked" : "");
        $email_relevant_stories = ($result[0]->email_relevant_stories == 1 ? "checked" : "");
    }
    ?>
    <section>
        <div class="blog_category_sticky profile_setting">
            <div class="container">
                <div class="blog_category_sticky_left">
                    <div class="desktop_catagory">
                        <div class="category_full_list">
                            <div class="category_btn">
                                <div class="category_btn_icon">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/cat_icon.png" class="img-responsive">
                                </div>
                                <div class="category_btn_txt">
                                    <span>Settings</span>
                                </div>
                            </div>
                            <ul id="links">
                                <li id="note_1"  class="active">Profile settings</li>                           
                                <li id="note_2" >Get paid </li>
                                <li id="note_4" > Notification settings</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="blog_category_main" id="notContent">
            <div class="container">
                <div id="notContent_1" class="blog_listt">
                    <div class="row">
                        <?php if ($profile_pic): ?>
                            <div class="col-sm-2">
                                <div class="main_profile_img_block">
                                    <div class="main_profile_img">
                                        <img  src="<?php echo $profile_pic; ?>" alt="" id="profile_pic" class="image-responsive output" />
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="col-sm-10">
                            <div class="setting_right">
                                <div class="field_title">
                                    <h4>Personal info </h4>                           
                                </div>
                                <div class="all_fields">
                                    <div class="row">                               
                                        <div class="col-sm-6">
                                            <input type="text" data-fname="<?php echo $user_info->user_firstname; ?>" class="pro_info only_alpha" placeholder="First Name" id="fname" name="fname" value="<?php echo $user_info->user_firstname; ?>" disabled="">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text"  data-lname="<?php echo $user_info->user_lastname; ?>"  class="pro_info only_alpha"  id="lname" placeholder="Last Name" name="lname" value="<?php echo $user_info->user_lastname; ?>" disabled="true">
                                        </div>

                                        <div class="col-sm-6">
                                            <input type="text"  data-add1="<?php echo get_usermeta($user_id, 'address1', TRUE) ?>"   class="pro_info" placeholder="Address Line1*"  id="add1" name="add1" value="<?php echo get_usermeta($user_id, 'address1', TRUE) ?>"  disabled="true">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text"  data-add2="<?php echo get_usermeta($user_id, 'address2', TRUE) ?>"  class="pro_info" placeholder="Address Line2"  id="add2" name="add2" value="<?php echo get_usermeta($user_id, 'address2', TRUE) ?>"  disabled="true">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text"   data-zip_code="<?php echo get_usermeta($user_id, 'zip_code', TRUE) ?>"  class="pro_info only_num" placeholder="Zip Code" maxlength="6"  id="zip_code" name="zip_code" value="<?php echo get_usermeta($user_id, 'zip_code', TRUE) ?>"  disabled="true" >
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text"   data-P_state="<?php echo get_usermeta($user_id, 'state', TRUE) ?>"  class="pro_info only_alpha" placeholder="State"  id="P_state" name="P_state" value="<?php echo get_usermeta($user_id, 'state', TRUE) ?>"  disabled="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="field_title">
                                    <h4>Education & University</h4>                              
                                </div>
                                <div class="all_fields">
                                    <?php $total_edu = get_user_meta($user_id, 'total_edu', TRUE); ?>
                                    <div class="row edu_parent">
                                        <?php
                                        if ($total_edu > 0):
                                            for ($i = 1; $i <= $total_edu; $i++) {
                                                ?>
                                                <div class="edu_info" id="uni_<?php echo $i; ?>">
                                                    <div class="col-sm-6">
                                                        <input type="text" data-degree="<?php echo get_usermeta($user_id, 'degree' . $i, TRUE) ?>" placeholder="Degree of" class="university_info edu_degree" id='degree<?php echo $i; ?>' name='degree<?php echo $i; ?>' value="<?php echo get_usermeta($user_id, 'degree' . $i, TRUE) ?>"  disabled="true">
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <input type="text" data-uni="<?php echo get_usermeta($user_id, 'university' . $i, TRUE) ?>" placeholder="University" class="university_info edu_uni" id='uni<?php echo $i; ?>' name='uni<?php echo $i; ?>' value="<?php echo get_usermeta($user_id, 'university' . $i, TRUE) ?>" disabled="true">
                                                    </div>                                            
                                                </div>
                                            <?php } endif; ?>
                                    </div>
                                </div>
                                <div class="field_title">
                                    <h4>Country info</h4>
                                </div>
                                <div class="all_fields">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input data-country_citizenship="<?php echo get_usermeta($user_id, 'country_citizenship', TRUE) ?>" class="country_info only_alpha"  type="text" id='country_citizenship' name='country_citizenship' value="<?php echo get_usermeta($user_id, 'country_citizenship', TRUE) ?>" placeholder="Country of Citizenship" disabled="true">
                                        </div>
                                        <div class="col-sm-6">
                                            <input data-country_cresidence="<?php echo get_usermeta($user_id, 'country_cresidence', TRUE) ?>" class="country_info only_alpha"  type="text" name='country_cresidence' id='country_cresidence' value="<?php echo get_usermeta($user_id, 'country_cresidence', TRUE) ?>" placeholder="Country of residence" disabled="true">
                                        </div>
                                    </div>                            
                                </div>
                                <div class="field_title">
                                    <h4>Job History</h4>                            
                                </div>
                                <div class="all_fields">
                                    <div class="row ">
                                        <?php
                                        $jobs = $wpdb->get_results("SELECT * FROM job_history WHERE proof_id = $user_id");
                                        $total_job = count($jobs);
                                        ?>  
                                        <div class="job_table">
                                            <div class="job_parent">
                                                <?php
                                                if ($total_job > 0):
                                                    $count = 0;
                                                    foreach ($jobs as $job) {
                                                        $count++;
                                                        ?>
                                                        <div class="job_history" id="job_<?php echo $count; ?>">

                                                            <div class="col-sm-6">
                                                                <input  disabled="true" value="<?php echo $job->company_name; ?>"  type="text" data-cname="<?php echo $job->company_name; ?>" placeholder="company Name" class="job_info job_cname contact_block" id='cname<?php echo $count; ?>' name='cname[]'   >
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <input  disabled="true"  value="<?php echo $job->designation; ?>" type="text" data-designation="<?php echo $job->designation; ?>" placeholder="Designation" class="job_info job_des contact_block" id='designation<?php echo $count; ?>' name='designation[]' >
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <input  disabled="true" value="<?php echo date("d-m-Y", strtotime($job->start_date)); ?>"   type="text" data-start_date="<?php echo date("d-m-Y", strtotime($job->start_date)); ?>" placeholder="Start date" class="job_date job_info job_sdate contact_block" id='sdate<?php echo $count; ?>' name='sdate[]'   >
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <input  disabled="true"  value="<?php echo date("d-m-Y", strtotime($job->end_date)); ?>"  type="text" data-end_date="<?php echo date("d-m-Y", strtotime($job->end_date)); ?>" placeholder="End date" class="job_date job_info job_edate contact_block" id='edate<?php echo $count; ?>' name='edate[]'   >
                                                            </div>

                                                        </div>
                                                    <?php } endif; ?>                                               
                                            </div>
                                            <div class="col-sm-6">                                       
                                                <?php
                                                if (get_user_meta($user_id, 'job_resume', true)):
                                                    echo '<div class="resume_div">';
                                                    $url = get_user_meta($user_id, 'job_resume', true);
                                                    echo 'Uploaded Resume: <a href="' . $url . '" target="_blank">' . $name = basename($url) . '</a>';

                                                    echo '</div>';
                                                endif;
                                                ?>

                                            </div>
                                        </div>                               
                                    </div>

                                </div>

                                <div class="field_title">
                                    <h4>Email</h4>
                                </div>
                                <div class="all_fields">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input type="text" value="<?php echo $user_info->user_email; ?>" disabled="true">
                                        </div>
                                    </div>
                                </div>  
                            </div>                   
                        </div>
                    </div>
                </div>


                <div id="notContent_2" class="blog_listt">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="setting_right">
                                <div class="field_title">
                                    <h4>Paypal ID </h4>
                                </div>
                            </div>
                            <div class="all_fields">
                                <div class="row">
                                    <div class="col-sm-6">                                       
                                        <input type="email" class="paypal_info contact_block" name='paid_id' id='paid_id' value="<?php echo $user_tax[0]->Paypal_ID; ?>" placeholder="Email" disabled="true">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="notContent_3" class="blog_listt">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="setting_right notification_settings">
                                <div class="field_title">
                                    <h4>Receive a desktop notification </h4>
                                </div>
                                <div class="setting_checkbox">
                                    <div class="s_checkbox">
                                        <input disabled="" id="checkbox-1" <?php echo $desktop_new_doc; ?> class="checkbox-custom" name="desktop_new_doc" type="checkbox">
                                        <label for="checkbox-1" class="checkbox-custom-label">There are new documents to review </label>
                                    </div>
                                    <div class="s_checkbox">
                                        <input disabled="" id="checkbox-2" <?php echo $desktop_other_notification; ?> class="checkbox-custom" name="desktop_other_notification" type="checkbox">
                                        <label for="checkbox-2" class="checkbox-custom-label">Other relevant notifications letting you know about the availability of documents to proofread </label>
                                    </div>
                                </div>
                                <div class="field_title">
                                    <h4>Receive an email</h4>
                                </div>
                                <div class="setting_checkbox last_field_checkbox">
                                    <div class="s_checkbox">
                                        <input disabled="" id="checkbox-3" <?php echo $email_info_about_availability; ?> class="checkbox-custom" name="email_info_about_availability" type="checkbox">
                                        <label for="checkbox-3" class="checkbox-custom-label">We have information about the availability of documents to proofread </label>
                                    </div>
                                    <div class="s_checkbox">
                                        <input disabled="" id="checkbox-4" <?php echo $email_relevant_stories; ?> class="checkbox-custom" name="email_relevant_stories" type="checkbox">
                                        <label for="checkbox-4" class="checkbox-custom-label">We publish stories relevant to your interests</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <?php
} if ($user_role == 'customer') {
    $paypal_result = $wpdb->get_results("SELECT * FROM tbl_customer_general_info WHERE fk_customer_id = $user_id LIMIT 1 ");

    $result = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_notification_settings WHERE user_id = %d ", $user_id));

    $dash_doc_started = "";
    $dash_doc_completed = "";
    $receive_doc_started = "";
    $receive_doc_completed = "";
    $receive_stories = "";


    if (count($result) > 0) {
        $dash_doc_started = ($result[0]->dash_doc_started == 1 ? "checked" : "");
        $dash_doc_completed = ($result[0]->dash_doc_completed == 1 ? "checked" : "");
        $receive_doc_started = ($result[0]->receive_doc_started == 1 ? "checked" : "");
        $receive_doc_completed = ($result[0]->receive_doc_completed == 1 ? "checked" : "");
        $receive_stories = ($result[0]->receive_stories == 1 ? "checked" : "");
    }
    ?>

    <section>
        <div id="main_blog" class="profile">
            <div class="blog_category_sticky">
                <div class="container">
                    <div class="blog_category_sticky_left">
                        <div class="desktop_catagory">
                            <div class="category_full_list">
                                <div class="category_btn">
                                    <div class="category_btn_icon">
                                        <img src="<?php echo get_template_directory_uri() ?>/images/cat_icon.png" class="img-responsive">
                                    </div>
                                    <div class="category_btn_txt">
                                        <span>settings</span>
                                    </div>
                                </div>
                                <ul id="links">
                                    <li id="note_1" class="active"> Profile settings </li>
                                    <li id="note_2" > Subscription settings</li>
                                    <li id="note_3"> Billing settings</li>
                                    <li id="note_4"> Notification settings</li>
                                    <li id="note_5">Manage Credits</li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="blog_category_main" id="notContent">
                <div class="container">
                    <div id="notContent_1" class="blog_listt">
                        <div class="row">
                            <?php if ($profile_pic): ?>
                                <div class="col-sm-2">
                                    <div class="main_profile_img_block">
                                        <div class="main_profile_img">
                                            <img  src="<?php echo $profile_pic; ?>" alt="" id="profile_pic" class="image-responsive output" />
                                        </div>
                                    </div>
                                </div>  
                            <?php endif; ?>
                            <div class="col-sm-10">
                                <div class="setting_right">
                                    <div class="field_title">
                                        <h4>Personal info </h4>                           
                                    </div>
                                    <div class="all_fields">
                                        <div class="row">                               
                                            <div class="col-sm-6">
                                                <input type="text" data-fname="<?php echo $user_info->user_firstname; ?>" class="pro_info only_alpha" placeholder="First Name" id="fname" name="fname" value="<?php echo $user_info->user_firstname; ?>" disabled="">
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text"  data-lname="<?php echo $user_info->user_lastname; ?>"  class="pro_info only_alpha"  id="lname" placeholder="Last Name" name="lname" value="<?php echo $user_info->user_lastname; ?>" disabled="true">
                                            </div> 
                                        </div>                                
                                    </div>  
                                    <div class="field_title">
                                        <h4>Email</h4>
                                    </div>
                                    <div class="all_fields">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input type="text" value="<?php echo $user_info->user_email; ?>" disabled="true">
                                            </div>
                                        </div>
                                    </div>  
                                </div>                   
                            </div>
                        </div>
                    </div>
                    <div id="notContent_2" class="blog_listt">
                        <?php
                        $user_info = $wpdb->get_results("SELECT * FROM tbl_customer_general_info WHERE fk_customer_id =$user_id LIMIT 1");
                        $membership_level = pmpro_getMembershipLevelForUser($user_id);
                        ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <h4>Current plan</h4>
                                <div class="total_ammount credit current <?php echo (empty($membership_level)) ? "not_found" : ""; ?>">
                                    <?php if (!empty($membership_level)): ?>
                                        <div class="left">
                                            <h4><?php echo $membership_level->plan_words; ?><span> Words <?php echo ($membership_level->expiration_number > 1) ? '/' . $membership_level->expiration_number : ''; ?><?php echo $membership_level->expiration_period; ?></span></h4>
                                        </div>
                                        <div class="right_cont">
                                            <h4>$<?php echo $membership_level->initial_payment; ?></h4>
                                            <p>Currently a '<?php echo $membership_level->name; ?>' subscriber. </p>

                                        </div> 

                                        <?php
                                    else:
                                        echo "<p>User have no plan purchased yet..</p>";
                                    endif;
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-6 privacy customer">
                                <h4>Remaining words</h4>
                                <div class="total_ammount credit">
                                    <div class="left">
                                        <h4><?php echo ($user_info[0]->remaining_credit_words) ? $user_info[0]->remaining_credit_words : '0'; ?><span> Words</span></h4>
                                        <p>Remaining credit</p>
                                    </div>
                                    <div class="right"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="notContent_3" class="blog_listt">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="setting_right">
                                    <div class="field_title">
                                        <h4>Paypal ID </h4>
                                    </div>
                                </div>
                                <div class="all_fields">
                                    <div class="row">
                                        <div class="col-sm-6">                                       
                                            <input disabled="" value="<?php echo $paypal_result[0]->paypal_id; ?>" type="email" class="contact_block" name="paypal_id" id="paypal_id"  maxlength="100"  placeholder="Paypal Id">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="notContent_4" class="blog_listt">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="setting_right">
                                    <div class="field_title">
                                        <h4>Dashboard notification</h4>
                                    </div>
                                    <div class="setting_checkbox">
                                        <div class="s_checkbox">
                                            <input disabled="" id="checkbox-1"  <?php echo $dash_doc_started ?> class="checkbox-custom" name="checkbox-1" type="checkbox" data-id="dash_doc_started">
                                            <label for="checkbox-1" class="checkbox-custom-label">A document you submitted has been started</label>
                                        </div>
                                        <div class="s_checkbox">
                                            <input disabled="" id="checkbox-2"   <?php echo $dash_doc_completed; ?> class="checkbox-custom" name="checkbox-2" type="checkbox" data-id="dash_doc_completed">
                                            <label for="checkbox-2" class="checkbox-custom-label">A document you submitted has been completed</label>
                                        </div>
                                    </div>
                                    <div class="field_title">
                                        <h4>Receive notification via email</h4>
                                    </div>
                                    <div class="setting_checkbox last_field_checkbox">
                                        <div class="s_checkbox">
                                            <input disabled="" id="checkbox-3"  <?php echo $receive_doc_started ?>  class="checkbox-custom" name="checkbox-3" type="checkbox" data-id="receive_doc_started">
                                            <label for="checkbox-3" class="checkbox-custom-label">A document you submitted has been started </label>
                                        </div>
                                        <div class="s_checkbox">
                                            <input disabled="" id="checkbox-4"  <?php echo $receive_doc_completed ?> class="checkbox-custom" name="checkbox-4" type="checkbox" data-id="receive_doc_completed">
                                            <label for="checkbox-4" class="checkbox-custom-label">A document you submitted has been completed</label>
                                        </div>
                                        <div class="s_checkbox">
                                            <input disabled="" id="checkbox-5"   <?php echo $receive_stories ?> class="checkbox-custom" name="checkbox-5" type="checkbox" data-id="receive_stories">
                                            <label for="checkbox-5" class="checkbox-custom-label">We publish stories relevant to your interests</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="notContent_5" class="blog_listt">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="setting_right">
                                    <div class="field_title">
                                        <h4>Remaining Credit Words</h4>
                                    </div>
                                </div>
                                <div class="all_fields">
                                    <div class="row credit_result">
                                        <div class="col-sm-6 "> 
                                            <input type="hidden" id="cust_name" value="<?php echo $user_id; ?>" />
                                            <input  value="<?php echo $wpdb->get_var("SELECT remaining_credit_words FROM tbl_customer_general_info WHERE fk_customer_id= $user_id"); ?>" type="text"  name="credits" id="credits"  class="contact_block credits only_num" maxlength="20">
                                        </div>
                                        <div class="col-sm-6 ">
                                            <input type="button" value="Update credit" class="btn_sky update_credit" />      
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>  
<?php } ?>
</div>
<script>

    jQuery('.update_credit').live('click', function () {
        debugger;
        $('.credit_err').remove();
        var cust_id = $('#cust_name').val();
        var credits = $('.credits').val();
        if (credits == '') {
            $('.credits').after('<span class="credit_err text-danger">Enter Credit</span>');
            return false;
        }

        $('.load_overlay').show();
        $.ajax({
            type: 'POST',
            url: '<?php echo admin_url('admin-ajax.php'); ?>',
            method: "post",
            data: {
                action: 'update_cust_credit',
                cust_id: cust_id,
                credits: credits
            },
            type: 'POST',
            success: function (data) {
                debugger;
                console.log(data);
                if (data)
                {
                    $('.credit_result').after('<span class="credit_err text-success">Credit upadeted sucessfully...</span>');
                } else
                {
                    $('.credit_result').after('<span class="credit_err text-danger">Credit not upadeted sucessfully...</span>');
                }
                $(".credit_err").fadeOut(2000);
                $('.load_overlay').hide();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('.load_overlay').hide();
                console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
            }
        });
    });

</script>