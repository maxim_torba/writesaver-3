<?php
/*
 *  Writesaver List
 */
wp_enqueue_style('admin-custom-bootstrap', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/bootstrap.min.css', '', 'all');
wp_enqueue_style('admin-font-style', get_template_directory_uri() . '/css/font-awesome.css', '', '', 'all');
wp_enqueue_style('admin-datatable-style', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/jquery.dataTables.min.css', '', '', 'all');
wp_enqueue_style('admin-custom-style', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/style.css', '', '', 'all');
wp_enqueue_script('admin-custom-bootstrap-js', WRITESAVER_CUSTOM_PLUGIN_URL . '/js/admin/bootstrap.min.js', array('jquery'), '', 'all');
wp_enqueue_script('admin-datatable-script', WRITESAVER_CUSTOM_PLUGIN_URL . '/js/admin/jquery.dataTables.min.js', array('jquery'), '', true);
wp_enqueue_script('admin-jquery-js', WRITESAVER_CUSTOM_PLUGIN_URL . '/js/admin/custom.js', array('jquery'), '', 'all');
?>
<div class="load_overlay" id="loding">
    <img src="<?php echo get_template_directory_uri(); ?>/images/39.gif"/>
</div>
<div class="proof_list" id="proof_list">
    <h1>Proofreader List</h1> 
    <?php
    $args = array(
        'role' => 'proofreader',
        'count_total' => true,
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key' => 'test_completed',
                'value' => TRUE,
                'compare' => '='
            ),
            array(
                'relation' => 'OR',
                array(
                    'key' => 'test_status',
                    'compare' => 'NOT EXISTS'
                ),
                array(
                    'key' => 'test_status',
                    'value' => Array('accepted', 'rejected'),
                    'compare' => 'NOT IN'
                )
            )
        )
    );

    $proofusers = get_users($args);
    ?>
    <table class="table" id="list_table">
        <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($proofusers) {
                foreach ($proofusers as $user) {
                    $user_id = $user->ID;
                    $test_status = get_user_meta($user_id, 'test_status', TRUE);
                    ?>

                    <tr>
                        <td><?php echo $user->first_name; ?></td>                        
                        <td><?php echo $user->last_name; ?></td>
                        <td><?php echo $user->user_email; ?></td>
                        <td>
                            <div class="test_status">

                                <select class="status" <?php echo ($test_status == 'rejected') ? 'style="pointer-events:none";' : ''; ?>>
                                    <option value="">Select Status</option>
                                    <option <?php echo ($test_status == 'accepted') ? 'selected' : ''; ?> value="accepted">Accepted</option>
                                    <option  <?php echo ($test_status == 'delayed') ? 'selected' : ''; ?>  value="delayed">Delayed</option>
                                    <option   <?php echo ($test_status == 'rejected') ? 'selected' : ''; ?>  value="rejected">Rejected</option>
                                </select>
                                <input type="hidden" class="proof_id" value="<?php echo $user_id; ?>" />
                                <div class="status_msg"></div>
                            </div>
                        </td>
                        <td>
                            <a href="<?php echo site_url(); ?>/wp-admin/admin.php?page=proofreader_view&proof_id=<?php echo $user_id; ?>" ><i class="fa fa-eye" aria-hidden="true"></i></a>                      
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
    <?php
    ?>
</div>
<script>
    jQuery(document).ready(function () {
        jQuery('#list_table').DataTable({
            "oLanguage": {
                "sEmptyTable": "No proofreader available."
            }
        });

        jQuery('.status').change(function (event) {

            var button = jQuery(this);
            jQuery('.statusmsg').remove();
            var status = jQuery(this).val();
            var proof_id = button.next('.proof_id').val();
            if (status) {
                var r = confirm("Are you sure to change status to " + status + "?");
                if (r == true) {
                    jQuery('#loding').show();
                    jQuery.ajax({
                        url: "<?php echo admin_url('admin-ajax.php'); ?>",
                        type: "POST",
                        data: {
                            action: 'change_proof_status',
                            proof_id: proof_id,
                            status: status
                        },
                        success: function (data) {
                            debugger;
                            jQuery('#loding').hide();
                            if (data == 1)
                                button.parent('div.test_status').find('.status_msg').html('<span class="text-success statusmsg">Request ' + status + ' sucessfully... </span>');
                            else
                                button.parent('div.test_status').find('.status_msg').html('<span class="text-danger statusmsg">Request not ' + status + ' sucessfully...</span>');
                            jQuery(".statusmsg").fadeOut(5000);

                            if (status == 'rejected' || status == 'accepted') {
                                button.css('pointer-events', 'none');
                                location.reload();
                            }

                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            jQuery('#loding').hide();
                            console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                        }
                    });
                } else {
                    $(r).dialog("close");
                }
            }
            return false;
        });
    });
</script>

